﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using cloudscribe.Pagination.Models;
using IPT_101.Repository.Models;
namespace IPT_101.ViewModels
{
    public class ProductListViewModel
    {
        public ProductListViewModel()
        {
            Products = new PagedResult<ProductDetail>();
        }

        public string Query { get; set; } = string.Empty;

        public PagedResult<ProductDetail> Products { get; set; } = null;
    }
}
