﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using IPT_101.Repository.Models;

#nullable disable

namespace IPT_101.Repository
{
    public partial class IPT_101Context : DbContext
    {
        public IPT_101Context()
        {
        }

        public IPT_101Context(DbContextOptions<IPT_101Context> options)
            : base(options)
        {
        }

        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<ProductDetail> ProductDetails { get; set; }
        public virtual DbSet<ProductType> ProductTypes { get; set; }
        public virtual DbSet<Register> Registers { get; set; }
        public virtual DbSet<ShoppingCart> ShoppingCarts { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Data Source=HOGAN;Initial Catalog=IPT_101;Integrated Security=True");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var rand = new Random();
            modelBuilder.Entity<Product>().HasData(
                new Product
                {
                    Id = 1,
                    ProductCode = "Prod-1-6292021",
                    ProductName = "Oppo A74",
                    ProductDescription = "This phone will make you boom-boom.",
                    SuggestedUnitPrice = rand.Next(9999, 99999) / 10,
                    UnitsInStock = rand.Next(0, 999),
                    ParcelPreparationInDays = 5,
                    IsActive = 1,
                },
                new Product
                {
                    Id = 2,
                    ProductCode = "Prod-2-6292021",
                    ProductName = "Oppo A92",
                    ProductDescription = "This phone will make you boom-boom.",
                    SuggestedUnitPrice = rand.Next(9999, 99999) / 10,
                    UnitsInStock = rand.Next(0, 999),
                    ParcelPreparationInDays = 5,
                    IsActive = 1,
                },
                new Product
                {
                    Id = 3,
                    ProductCode = "Prod-3-6292021",
                    ProductName = "Oppo A94",
                    ProductDescription = "This phone will make you boom-boom.",
                    SuggestedUnitPrice = rand.Next(9999, 99999) / 10,
                    UnitsInStock = rand.Next(0, 999),
                    ParcelPreparationInDays = 5,
                    IsActive = 1,
                },
                new Product
                {
                    Id = 4,
                    ProductCode = "Prod-4-6292021",
                    ProductName = "Oppo Reno 3 Pro",
                    ProductDescription = "This phone will make you boom-boom.",
                    SuggestedUnitPrice = rand.Next(9999, 99999) / 10,
                    UnitsInStock = rand.Next(0, 999),
                    ParcelPreparationInDays = 5,
                    IsActive = 1,
                },
                new Product
                {
                    Id = 5,
                    ProductCode = "Prod-5-6292021",
                    ProductName = "Oppo Reno 5",
                    ProductDescription = "This phone will make you boom-boom.",
                    SuggestedUnitPrice = rand.Next(9999, 99999) / 10,
                    UnitsInStock = rand.Next(0, 999),
                    ParcelPreparationInDays = 5,
                    IsActive = 1,
                },
                new Product
                {
                    Id = 6,
                    ProductCode = "Prod-6-6292021",
                    ProductName = "Oppo A5S",
                    ProductDescription = "This phone will make you boom-boom.",
                    SuggestedUnitPrice = rand.Next(9999, 99999) / 10,
                    UnitsInStock = rand.Next(0, 999),
                    ParcelPreparationInDays = 5,
                    IsActive = 1,
                },
                new Product
                {
                    Id = 7,
                    ProductCode = "Prod-7-6292021",
                    ProductName = "Oppo Reno 4",
                    ProductDescription = "This phone will make you boom-boom.",
                    SuggestedUnitPrice = rand.Next(9999, 99999) / 10,
                    UnitsInStock = rand.Next(0, 999),
                    ParcelPreparationInDays = 5,
                    IsActive = 1,
                },
                new Product
                {
                    Id = 8,
                    ProductCode = "Prod-8-6292021",
                    ProductName = "Oppo A15S",
                    ProductDescription = "This phone will make you boom-boom.",
                    SuggestedUnitPrice = rand.Next(9999, 99999) / 10,
                    UnitsInStock = rand.Next(0, 999),
                    ParcelPreparationInDays = 5,
                    IsActive = 1,
                }
                ,
                new Product
                {
                    Id = 9,
                    ProductCode = "Prod-9-6292021",
                    ProductName = "Oppo A12",
                    ProductDescription = "This phone will make you boom-boom.",
                    SuggestedUnitPrice = rand.Next(9999, 99999) / 10,
                    UnitsInStock = rand.Next(0, 999),
                    ParcelPreparationInDays = 5,
                    IsActive = 1,
                },
                new Product
                {
                    Id = 10,
                    ProductCode = "Prod-10-6292021",
                    ProductName = "Oppo Reno 2F",
                    ProductDescription = "This phone will make you boom-boom.",
                    SuggestedUnitPrice = rand.Next(9999, 99999) / 10,
                    UnitsInStock = rand.Next(0, 999),
                    ParcelPreparationInDays = 5,
                    IsActive = 1,
                },
                new Product
                {
                    Id = 11,
                    ProductCode = "Prod-11-6292021",
                    ProductName = "Samsung Galaxy A50S",
                    ProductDescription = "This phone will make you boom-boom.",
                    SuggestedUnitPrice = rand.Next(9999, 99999) / 10,
                    UnitsInStock = rand.Next(0, 999),
                    ParcelPreparationInDays = 5,
                    IsActive = 1,
                },
                new Product
                {
                    Id = 12,
                    ProductCode = "Prod-12-6292021",
                    ProductName = "Samsung Galaxy A01",
                    ProductDescription = "This phone will make you boom-boom.",
                    SuggestedUnitPrice = rand.Next(9999, 99999) / 10,
                    UnitsInStock = rand.Next(0, 999),
                    ParcelPreparationInDays = 5,
                    IsActive = 1,
                },
                new Product
                {
                    Id = 13,
                    ProductCode = "Prod-13-6292021",
                    ProductName = "Samsung Galaxy A11",
                    ProductDescription = "This phone will make you boom-boom.",
                    SuggestedUnitPrice = rand.Next(9999, 99999) / 10,
                    UnitsInStock = rand.Next(0, 999),
                    ParcelPreparationInDays = 5,
                    IsActive = 1,
                },
                new Product
                {
                    Id = 14,
                    ProductCode = "Prod-14-6292021",
                    ProductName = "Samsung Galaxy S20+",
                    ProductDescription = "This phone will make you boom-boom.",
                    SuggestedUnitPrice = rand.Next(9999, 99999) / 10,
                    UnitsInStock = rand.Next(0, 999),
                    ParcelPreparationInDays = 5,
                    IsActive = 1,
                },
                new Product
                {
                    Id = 15,
                    ProductCode = "Prod-15-6292021",
                    ProductName = "Samsung Galaxy Note 20",
                    ProductDescription = "This phone will make you boom-boom.",
                    SuggestedUnitPrice = rand.Next(9999, 99999) / 10,
                    UnitsInStock = rand.Next(0, 999),
                    ParcelPreparationInDays = 5,
                    IsActive = 1,
                },
                new Product
                {
                    Id = 16,
                    ProductCode = "Prod-16-6292021",
                    ProductName = "Samsung Galaxy Note 10+",
                    ProductDescription = "This phone will make you boom-boom.",
                    SuggestedUnitPrice = rand.Next(9999, 99999) / 10,
                    UnitsInStock = rand.Next(0, 999),
                    ParcelPreparationInDays = 5,
                    IsActive = 1,
                },
                new Product
                {
                    Id = 17,
                    ProductCode = "Prod-17-6292021",
                    ProductName = "Samsung Galaxy A30S",
                    ProductDescription = "This phone will make you boom-boom.",
                    SuggestedUnitPrice = rand.Next(9999, 99999) / 10,
                    UnitsInStock = rand.Next(0, 999),
                    ParcelPreparationInDays = 5,
                    IsActive = 1,
                },
                new Product
                {
                    Id = 18,
                    ProductCode = "Prod-18-6292021",
                    ProductName = "Samsung Galaxy A71",
                    ProductDescription = "This phone will make you boom-boom.",
                    SuggestedUnitPrice = rand.Next(9999, 99999) / 10,
                    UnitsInStock = rand.Next(0, 999),
                    ParcelPreparationInDays = 5,
                    IsActive = 1,
                },
                new Product
                {
                    Id = 19,
                    ProductCode = "Prod-19-6292021",
                    ProductName = "Samsung Galaxy A72",
                    ProductDescription = "This phone will make you boom-boom.",
                    SuggestedUnitPrice = rand.Next(9999, 99999) / 10,
                    UnitsInStock = rand.Next(0, 999),
                    ParcelPreparationInDays = 5,
                    IsActive = 1,
                },
                new Product
                {
                    Id = 20,
                    ProductCode = "Prod-20-6292021",
                    ProductName = "Samsung Galaxy A52",
                    ProductDescription = "This phone will make you boom-boom.",
                    SuggestedUnitPrice = rand.Next(9999, 99999) / 10,
                    UnitsInStock = rand.Next(0, 999),
                    ParcelPreparationInDays = 5,
                    IsActive = 1,
                },
                new Product
                {
                    Id = 21,
                    ProductCode = "Prod-21-6292021",
                    ProductName = "Apple iPhone 6s",
                    ProductDescription = "This phone will make you boom-boom.",
                    SuggestedUnitPrice = rand.Next(9999, 99999) / 10,
                    UnitsInStock = rand.Next(0, 999),
                    ParcelPreparationInDays = 5,
                    IsActive = 1,
                },
                new Product
                {
                    Id = 22,
                    ProductCode = "Prod-22-6292021",
                    ProductName = "Apple iPhone SE",
                    ProductDescription = "This phone will make you boom-boom.",
                    SuggestedUnitPrice = rand.Next(9999, 99999) / 10,
                    UnitsInStock = rand.Next(0, 999),
                    ParcelPreparationInDays = 5,
                    IsActive = 1,
                },
                new Product
                {
                    Id = 23,
                    ProductCode = "Prod-23-6292021",
                    ProductName = "Apple iPhone 12 Pro Max",
                    ProductDescription = "This phone will make you boom-boom.",
                    SuggestedUnitPrice = rand.Next(9999, 99999) / 10,
                    UnitsInStock = rand.Next(0, 999),
                    ParcelPreparationInDays = 5,
                    IsActive = 1,
                },
                new Product
                {
                    Id = 24,
                    ProductCode = "Prod-24-6292021",
                    ProductName = "Apple iPhone 11",
                    ProductDescription = "This phone will make you boom-boom.",
                    SuggestedUnitPrice = rand.Next(9999, 99999) / 10,
                    UnitsInStock = rand.Next(0, 999),
                    ParcelPreparationInDays = 5,
                    IsActive = 1,
                },
                new Product
                {
                    Id = 25,
                    ProductCode = "Prod-25-6292021",
                    ProductName = "Apple iPhone 11 Pro",
                    ProductDescription = "This phone will make you boom-boom.",
                    SuggestedUnitPrice = rand.Next(9999, 99999) / 10,
                    UnitsInStock = rand.Next(0, 999),
                    ParcelPreparationInDays = 5,
                    IsActive = 1,
                },
                new Product
                {
                    Id = 26,
                    ProductCode = "Prod-26-6292021",
                    ProductName = "Apple iPhone 11 Pro Max",
                    ProductDescription = "This phone will make you boom-boom.",
                    SuggestedUnitPrice = rand.Next(9999, 99999) / 10,
                    UnitsInStock = rand.Next(0, 999),
                    ParcelPreparationInDays = 5,
                    IsActive = 1,
                },
                new Product
                {
                    Id = 27,
                    ProductCode = "Prod-27-6292021",
                    ProductName = "Apple iPhone 12",
                    ProductDescription = "This phone will make you boom-boom.",
                    SuggestedUnitPrice = rand.Next(9999, 99999) / 10,
                    UnitsInStock = rand.Next(0, 999),
                    ParcelPreparationInDays = 5,
                    IsActive = 1,
                },
                new Product
                {
                    Id = 28,
                    ProductCode = "Prod-28-6292021",
                    ProductName = "Apple iPhone 7+",
                    ProductDescription = "This phone will make you boom-boom.",
                    SuggestedUnitPrice = rand.Next(9999, 99999) / 10,
                    UnitsInStock = rand.Next(0, 999),
                    ParcelPreparationInDays = 5,
                    IsActive = 1,
                },
                new Product
                {
                    Id = 29,
                    ProductCode = "Prod-29-6292021",
                    ProductName = "Apple iPhone 8",
                    ProductDescription = "This phone will make you boom-boom.",
                    SuggestedUnitPrice = rand.Next(9999, 99999) / 10,
                    UnitsInStock = rand.Next(0, 999),
                    ParcelPreparationInDays = 5,
                    IsActive = 1,
                },
                new Product
                {
                    Id = 30,
                    ProductCode = "Prod-30-6292021",
                    ProductName = "Apple iPhone XR",
                    ProductDescription = "This phone will make you boom-boom.",
                    SuggestedUnitPrice = rand.Next(9999, 99999) / 10,
                    UnitsInStock = rand.Next(0, 999),
                    ParcelPreparationInDays = 5,
                    IsActive = 1,
                },
                new Product
                {
                    Id = 31,
                    ProductCode = "Prod-31-6292021",
                    ProductName = "Realme C2 2020",
                    ProductDescription = "This phone will make you boom-boom.",
                    SuggestedUnitPrice = rand.Next(9999, 99999) / 10,
                    UnitsInStock = rand.Next(0, 999),
                    ParcelPreparationInDays = 5,
                    IsActive = 1,
                },
                new Product
                {
                    Id = 32,
                    ProductCode = "Prod-32-6292021",
                    ProductName = "Realme C21Y",
                    ProductDescription = "This phone will make you boom-boom.",
                    SuggestedUnitPrice = rand.Next(9999, 99999) / 10,
                    UnitsInStock = rand.Next(0, 999),
                    ParcelPreparationInDays = 5,
                    IsActive = 1,
                },
                new Product
                {
                    Id = 33,
                    ProductCode = "Prod-33-6292021",
                    ProductName = "Realme C15",
                    ProductDescription = "This phone will make you boom-boom.",
                    SuggestedUnitPrice = rand.Next(9999, 99999) / 10,
                    UnitsInStock = rand.Next(0, 999),
                    ParcelPreparationInDays = 5,
                    IsActive = 1,
                },
                new Product
                {
                    Id = 34,
                    ProductCode = "Prod-34-6292021",
                    ProductName = "Realme C12",
                    ProductDescription = "This phone will make you boom-boom.",
                    SuggestedUnitPrice = rand.Next(9999, 99999) / 10,
                    UnitsInStock = rand.Next(0, 999),
                    ParcelPreparationInDays = 5,
                    IsActive = 1,
                },
                new Product
                {
                    Id = 35,
                    ProductCode = "Prod-35-6292021",
                    ProductName = "Realme C11",
                    ProductDescription = "This phone will make you boom-boom.",
                    SuggestedUnitPrice = rand.Next(9999, 99999) / 10,
                    UnitsInStock = rand.Next(0, 999),
                    ParcelPreparationInDays = 5,
                    IsActive = 1,
                },
                new Product
                {
                    Id = 36,
                    ProductCode = "Prod-36-6292021",
                    ProductName = "Realme 8",
                    ProductDescription = "This phone will make you boom-boom.",
                    SuggestedUnitPrice = rand.Next(9999, 99999) / 10,
                    UnitsInStock = rand.Next(0, 999),
                    ParcelPreparationInDays = 5,
                    IsActive = 1,
                },
                new Product
                {
                    Id = 37,
                    ProductCode = "Prod-37-6292021",
                    ProductName = "Realme 7 PRO",
                    ProductDescription = "This phone will make you boom-boom.",
                    SuggestedUnitPrice = rand.Next(9999, 99999) / 10,
                    UnitsInStock = rand.Next(0, 999),
                    ParcelPreparationInDays = 5,
                    IsActive = 1,
                },
                new Product
                {
                    Id = 38,
                    ProductCode = "Prod-38-6292021",
                    ProductName = "Realme 7",
                    ProductDescription = "This phone will make you boom-boom.",
                    SuggestedUnitPrice = rand.Next(9999, 99999) / 10,
                    UnitsInStock = rand.Next(0, 999),
                    ParcelPreparationInDays = 5,
                    IsActive = 1,
                },
                new Product
                {
                    Id = 39,
                    ProductCode = "Prod-39-6292021",
                    ProductName = "Realme 6i",
                    ProductDescription = "This phone will make you boom-boom.",
                    SuggestedUnitPrice = rand.Next(9999, 99999) / 10,
                    UnitsInStock = rand.Next(0, 999),
                    ParcelPreparationInDays = 5,
                    IsActive = 1,
                },
                new Product
                {
                    Id = 40,
                    ProductCode = "Prod-40-6292021",
                    ProductName = "Realme 6 PRO",
                    ProductDescription = "This phone will make you boom-boom.",
                    SuggestedUnitPrice = rand.Next(9999, 99999) / 10,
                    UnitsInStock = rand.Next(0, 999),
                    ParcelPreparationInDays = 5,
                    IsActive = 1,
                },
                new Product
                {
                    Id = 41,
                    ProductCode = "Prod-41-6292021",
                    ProductName = "Huawei P30 PRO",
                    ProductDescription = "This phone will make you boom-boom.",
                    SuggestedUnitPrice = rand.Next(9999, 99999) / 10,
                    UnitsInStock = rand.Next(0, 999),
                    ParcelPreparationInDays = 5,
                    IsActive = 1,
                },
                new Product
                {
                    Id = 42,
                    ProductCode = "Prod-42-6292021",
                    ProductName = "Huawei MATE 30 PRO",
                    ProductDescription = "This phone will make you boom-boom.",
                    SuggestedUnitPrice = rand.Next(9999, 99999) / 10,
                    UnitsInStock = rand.Next(0, 999),
                    ParcelPreparationInDays = 5,
                    IsActive = 1,
                },
                new Product
                {
                    Id = 43,
                    ProductCode = "Prod-43-6292021",
                    ProductName = "Huawei MATE 30",
                    ProductDescription = "This phone will make you boom-boom.",
                    SuggestedUnitPrice = rand.Next(9999, 99999) / 10,
                    UnitsInStock = rand.Next(0, 999),
                    ParcelPreparationInDays = 5,
                    IsActive = 1,
                },
                new Product
                {
                    Id = 44,
                    ProductCode = "Prod-44-6292021",
                    ProductName = "Huawei NOVA 5T",
                    ProductDescription = "This phone will make you boom-boom.",
                    SuggestedUnitPrice = rand.Next(9999, 99999) / 10,
                    UnitsInStock = rand.Next(0, 999),
                    ParcelPreparationInDays = 5,
                    IsActive = 1,
                },
                new Product
                {
                    Id = 45,
                    ProductCode = "Prod-45-6292021",
                    ProductName = "Huawei NOVA 7i",
                    ProductDescription = "This phone will make you boom-boom.",
                    SuggestedUnitPrice = rand.Next(9999, 99999) / 10,
                    UnitsInStock = rand.Next(0, 999),
                    ParcelPreparationInDays = 5,
                    IsActive = 1,
                },
                new Product
                {
                    Id = 46,
                    ProductCode = "Prod-46-6292021",
                    ProductName = "Huawei NOVA 7SE",
                    ProductDescription = "This phone will make you boom-boom.",
                    SuggestedUnitPrice = rand.Next(9999, 99999) / 10,
                    UnitsInStock = rand.Next(0, 999),
                    ParcelPreparationInDays = 5,
                    IsActive = 1,
                },
                new Product
                {
                    Id = 47,
                    ProductCode = "Prod-47-6292021",
                    ProductName = "Huawei P40",
                    ProductDescription = "This phone will make you boom-boom.",
                    SuggestedUnitPrice = rand.Next(9999, 99999) / 10,
                    UnitsInStock = rand.Next(0, 999),
                    ParcelPreparationInDays = 5,
                    IsActive = 1,
                },
                new Product
                {
                    Id = 48,
                    ProductCode = "Prod-48-6292021",
                    ProductName = "Huawei Y9S",
                    ProductDescription = "This phone will make you boom-boom.",
                    SuggestedUnitPrice = rand.Next(9999, 99999) / 10,
                    UnitsInStock = rand.Next(0, 999),
                    ParcelPreparationInDays = 5,
                    IsActive = 1,
                },
                new Product
                {
                    Id = 49,
                    ProductCode = "Prod-49-6292021",
                    ProductName = "Huawei Y8P",
                    ProductDescription = "This phone will make you boom-boom.",
                    SuggestedUnitPrice = rand.Next(9999, 99999) / 10,
                    UnitsInStock = rand.Next(0, 999),
                    ParcelPreparationInDays = 5,
                    IsActive = 1,
                },
                new Product
                {
                    Id = 50,
                    ProductCode = "Prod-50-6292021",
                    ProductName = "Huawei Y7P",
                    ProductDescription = "This phone will make you boom-boom.",
                    SuggestedUnitPrice = rand.Next(9999, 99999) / 10,
                    UnitsInStock = rand.Next(0, 999),
                    ParcelPreparationInDays = 5,
                    IsActive = 1,
                }

                );
            modelBuilder.Entity<ProductDetail>().HasData(
                new ProductDetail
                {
                    Id = 1,
                    ProductId = 1,
                    ProductTypeId = 1,
                    AdditionalInfo = "Define your Size, Color or Anything that occupied space and has mass.",
                    Image1 = "/images/Oppo/prod-1.png",
                    Image2 = "/images/DEFAULT_IMAGE.jpg",
                    Image3 = "/images/DEFAULT_IMAGE.jpg",
                    Image4 = "/images/DEFAULT_IMAGE.jpg",
                    Image5 = "/images/DEFAULT_IMAGE.jpg",
                    IsActive = 1,
                    UpdatedAt = DateTime.Now
                },
                 new ProductDetail
                 {
                     Id = 2,
                     ProductId = 2,
                     ProductTypeId = 1,
                     AdditionalInfo = "Define your Size, Color or Anything that occupied space and has mass.",
                     Image1 = "/images/Oppo/prod-2.png",
                     Image2 = "/images/DEFAULT_IMAGE.jpg",
                     Image3 = "/images/DEFAULT_IMAGE.jpg",
                     Image4 = "/images/DEFAULT_IMAGE.jpg",
                     Image5 = "/images/DEFAULT_IMAGE.jpg",
                     IsActive = 1,
                     UpdatedAt = DateTime.Now
                 },
                  new ProductDetail
                  {
                      Id = 3,
                      ProductId = 3,
                      ProductTypeId = 1,
                      AdditionalInfo = "Define your Size, Color or Anything that occupied space and has mass.",
                      Image1 = "/images/Oppo/prod-3.png",
                      Image2 = "/images/DEFAULT_IMAGE.jpg",
                      Image3 = "/images/DEFAULT_IMAGE.jpg",
                      Image4 = "/images/DEFAULT_IMAGE.jpg",
                      Image5 = "/images/DEFAULT_IMAGE.jpg",
                      IsActive = 1,
                      UpdatedAt = DateTime.Now
                  },
                   new ProductDetail
                   {
                       Id = 4,
                       ProductId = 4,
                       ProductTypeId = 1,
                       AdditionalInfo = "Define your Size, Color or Anything that occupied space and has mass.",
                       Image1 = "/images/Oppo/prod-4.png",
                       Image2 = "/images/DEFAULT_IMAGE.jpg",
                       Image3 = "/images/DEFAULT_IMAGE.jpg",
                       Image4 = "/images/DEFAULT_IMAGE.jpg",
                       Image5 = "/images/DEFAULT_IMAGE.jpg",
                       IsActive = 1,
                       UpdatedAt = DateTime.Now
                   },
                    new ProductDetail
                    {
                        Id = 5,
                        ProductId = 5,
                        ProductTypeId = 1,
                        AdditionalInfo = "Define your Size, Color or Anything that occupied space and has mass.",
                        Image1 = "/images/Oppo/prod-5.png",
                        Image2 = "/images/DEFAULT_IMAGE.jpg",
                        Image3 = "/images/DEFAULT_IMAGE.jpg",
                        Image4 = "/images/DEFAULT_IMAGE.jpg",
                        Image5 = "/images/DEFAULT_IMAGE.jpg",
                        IsActive = 1,
                        UpdatedAt = DateTime.Now
                    },
                     new ProductDetail
                     {
                         Id = 6,
                         ProductId = 6,
                         ProductTypeId = 1,
                         AdditionalInfo = "Define your Size, Color or Anything that occupied space and has mass.",
                         Image1 = "/images/Oppo/prod-6.png",
                         Image2 = "/images/DEFAULT_IMAGE.jpg",
                         Image3 = "/images/DEFAULT_IMAGE.jpg",
                         Image4 = "/images/DEFAULT_IMAGE.jpg",
                         Image5 = "/images/DEFAULT_IMAGE.jpg",
                         IsActive = 1,
                         UpdatedAt = DateTime.Now
                     },
                     new ProductDetail
                     {
                         Id = 7,
                         ProductId = 7,
                         ProductTypeId = 1,
                         AdditionalInfo = "Define your Size, Color or Anything that occupied space and has mass.",
                         Image1 = "/images/Oppo/prod-7.png",
                         Image2 = "/images/DEFAULT_IMAGE.jpg",
                         Image3 = "/images/DEFAULT_IMAGE.jpg",
                         Image4 = "/images/DEFAULT_IMAGE.jpg",
                         Image5 = "/images/DEFAULT_IMAGE.jpg",
                         IsActive = 1,
                         UpdatedAt = DateTime.Now
                     },
                     new ProductDetail
                     {
                         Id = 8,
                         ProductId = 8,
                         ProductTypeId = 1,
                         AdditionalInfo = "Define your Size, Color or Anything that occupied space and has mass.",
                         Image1 = "/images/Oppo/prod-8.png",
                         Image2 = "/images/DEFAULT_IMAGE.jpg",
                         Image3 = "/images/DEFAULT_IMAGE.jpg",
                         Image4 = "/images/DEFAULT_IMAGE.jpg",
                         Image5 = "/images/DEFAULT_IMAGE.jpg",
                         IsActive = 1,
                         UpdatedAt = DateTime.Now
                     },
                     new ProductDetail
                     {
                         Id = 9,
                         ProductId = 9,
                         ProductTypeId = 1,
                         AdditionalInfo = "Define your Size, Color or Anything that occupied space and has mass.",
                         Image1 = "/images/Oppo/prod-9.png",
                         Image2 = "/images/DEFAULT_IMAGE.jpg",
                         Image3 = "/images/DEFAULT_IMAGE.jpg",
                         Image4 = "/images/DEFAULT_IMAGE.jpg",
                         Image5 = "/images/DEFAULT_IMAGE.jpg",
                         IsActive = 1,
                         UpdatedAt = DateTime.Now
                     },
                     new ProductDetail
                     {
                         Id = 10,
                         ProductId = 10,
                         ProductTypeId = 1,
                         AdditionalInfo = "Define your Size, Color or Anything that occupied space and has mass.",
                         Image1 = "/images/Oppo/prod-10.png",
                         Image2 = "/images/DEFAULT_IMAGE.jpg",
                         Image3 = "/images/DEFAULT_IMAGE.jpg",
                         Image4 = "/images/DEFAULT_IMAGE.jpg",
                         Image5 = "/images/DEFAULT_IMAGE.jpg",
                         IsActive = 1,
                         UpdatedAt = DateTime.Now
                     },
                     new ProductDetail
                     {
                         Id = 11,
                         ProductId = 11,
                         ProductTypeId = 1,
                         AdditionalInfo = "Define your Size, Color or Anything that occupied space and has mass.",
                         Image1 = "/images/Samsung/prod-11.png",
                         Image2 = "/images/DEFAULT_IMAGE.jpg",
                         Image3 = "/images/DEFAULT_IMAGE.jpg",
                         Image4 = "/images/DEFAULT_IMAGE.jpg",
                         Image5 = "/images/DEFAULT_IMAGE.jpg",
                         IsActive = 1,
                         UpdatedAt = DateTime.Now
                     },
                     new ProductDetail
                     {
                         Id = 12,
                         ProductId = 12,
                         ProductTypeId = 1,
                         AdditionalInfo = "Define your Size, Color or Anything that occupied space and has mass.",
                         Image1 = "/images/Samsung/prod-12.png",
                         Image2 = "/images/DEFAULT_IMAGE.jpg",
                         Image3 = "/images/DEFAULT_IMAGE.jpg",
                         Image4 = "/images/DEFAULT_IMAGE.jpg",
                         Image5 = "/images/DEFAULT_IMAGE.jpg",
                         IsActive = 1,
                         UpdatedAt = DateTime.Now
                     },
                     new ProductDetail
                     {
                         Id = 13,
                         ProductId = 13,
                         ProductTypeId = 1,
                         AdditionalInfo = "Define your Size, Color or Anything that occupied space and has mass.",
                         Image1 = "/images/Samsung/prod-13.png",
                         Image2 = "/images/DEFAULT_IMAGE.jpg",
                         Image3 = "/images/DEFAULT_IMAGE.jpg",
                         Image4 = "/images/DEFAULT_IMAGE.jpg",
                         Image5 = "/images/DEFAULT_IMAGE.jpg",
                         IsActive = 1,
                         UpdatedAt = DateTime.Now
                     },
                     new ProductDetail
                     {
                         Id = 14,
                         ProductId = 14,
                         ProductTypeId = 1,
                         AdditionalInfo = "Define your Size, Color or Anything that occupied space and has mass.",
                         Image1 = "/images/Samsung/prod-14.png",
                         Image2 = "/images/DEFAULT_IMAGE.jpg",
                         Image3 = "/images/DEFAULT_IMAGE.jpg",
                         Image4 = "/images/DEFAULT_IMAGE.jpg",
                         Image5 = "/images/DEFAULT_IMAGE.jpg",
                         IsActive = 1,
                         UpdatedAt = DateTime.Now
                     },
                     new ProductDetail
                     {
                         Id = 15,
                         ProductId = 15,
                         ProductTypeId = 1,
                         AdditionalInfo = "Define your Size, Color or Anything that occupied space and has mass.",
                         Image1 = "/images/Samsung/prod-15.png",
                         Image2 = "/images/DEFAULT_IMAGE.jpg",
                         Image3 = "/images/DEFAULT_IMAGE.jpg",
                         Image4 = "/images/DEFAULT_IMAGE.jpg",
                         Image5 = "/images/DEFAULT_IMAGE.jpg",
                         IsActive = 1,
                         UpdatedAt = DateTime.Now
                     },
                     new ProductDetail
                     {
                         Id = 16,
                         ProductId = 16,
                         ProductTypeId = 1,
                         AdditionalInfo = "Define your Size, Color or Anything that occupied space and has mass.",
                         Image1 = "/images/Samsung/prod-16.png",
                         Image2 = "/images/DEFAULT_IMAGE.jpg",
                         Image3 = "/images/DEFAULT_IMAGE.jpg",
                         Image4 = "/images/DEFAULT_IMAGE.jpg",
                         Image5 = "/images/DEFAULT_IMAGE.jpg",
                         IsActive = 1,
                         UpdatedAt = DateTime.Now
                     },
                     new ProductDetail
                     {
                         Id = 17,
                         ProductId = 17,
                         ProductTypeId = 1,
                         AdditionalInfo = "Define your Size, Color or Anything that occupied space and has mass.",
                         Image1 = "/images/Samsung/prod-17.png",
                         Image2 = "/images/DEFAULT_IMAGE.jpg",
                         Image3 = "/images/DEFAULT_IMAGE.jpg",
                         Image4 = "/images/DEFAULT_IMAGE.jpg",
                         Image5 = "/images/DEFAULT_IMAGE.jpg",
                         IsActive = 1,
                         UpdatedAt = DateTime.Now
                     },
                     new ProductDetail
                     {
                         Id = 18,
                         ProductId = 18,
                         ProductTypeId = 1,
                         AdditionalInfo = "Define your Size, Color or Anything that occupied space and has mass.",
                         Image1 = "/images/Samsung/prod-18.png",
                         Image2 = "/images/DEFAULT_IMAGE.jpg",
                         Image3 = "/images/DEFAULT_IMAGE.jpg",
                         Image4 = "/images/DEFAULT_IMAGE.jpg",
                         Image5 = "/images/DEFAULT_IMAGE.jpg",
                         IsActive = 1,
                         UpdatedAt = DateTime.Now
                     },
                     new ProductDetail
                     {
                         Id = 19,
                         ProductId = 19,
                         ProductTypeId = 1,
                         AdditionalInfo = "Define your Size, Color or Anything that occupied space and has mass.",
                         Image1 = "/images/Samsung/prod-19.png",
                         Image2 = "/images/DEFAULT_IMAGE.jpg",
                         Image3 = "/images/DEFAULT_IMAGE.jpg",
                         Image4 = "/images/DEFAULT_IMAGE.jpg",
                         Image5 = "/images/DEFAULT_IMAGE.jpg",
                         IsActive = 1,
                         UpdatedAt = DateTime.Now
                     },
                     new ProductDetail
                     {
                         Id = 20,
                         ProductId = 20,
                         ProductTypeId = 1,
                         AdditionalInfo = "Define your Size, Color or Anything that occupied space and has mass.",
                         Image1 = "/images/Samsung/prod-20.png",
                         Image2 = "/images/DEFAULT_IMAGE.jpg",
                         Image3 = "/images/DEFAULT_IMAGE.jpg",
                         Image4 = "/images/DEFAULT_IMAGE.jpg",
                         Image5 = "/images/DEFAULT_IMAGE.jpg",
                         IsActive = 1,
                         UpdatedAt = DateTime.Now
                     },
                     new ProductDetail
                     {
                         Id = 21,
                         ProductId = 21,
                         ProductTypeId = 1,
                         AdditionalInfo = "Define your Size, Color or Anything that occupied space and has mass.",
                         Image1 = "/images/Apple/prod-21.png",
                         Image2 = "/images/DEFAULT_IMAGE.jpg",
                         Image3 = "/images/DEFAULT_IMAGE.jpg",
                         Image4 = "/images/DEFAULT_IMAGE.jpg",
                         Image5 = "/images/DEFAULT_IMAGE.jpg",
                         IsActive = 1,
                         UpdatedAt = DateTime.Now
                     },
                     new ProductDetail
                     {
                         Id = 22,
                         ProductId = 22,
                         ProductTypeId = 1,
                         AdditionalInfo = "Define your Size, Color or Anything that occupied space and has mass.",
                         Image1 = "/images/Apple/prod-22.png",
                         Image2 = "/images/DEFAULT_IMAGE.jpg",
                         Image3 = "/images/DEFAULT_IMAGE.jpg",
                         Image4 = "/images/DEFAULT_IMAGE.jpg",
                         Image5 = "/images/DEFAULT_IMAGE.jpg",
                         IsActive = 1,
                         UpdatedAt = DateTime.Now
                     },
                     new ProductDetail
                     {
                         Id = 23,
                         ProductId = 23,
                         ProductTypeId = 1,
                         AdditionalInfo = "Define your Size, Color or Anything that occupied space and has mass.",
                         Image1 = "/images/Apple/prod-23.png",
                         Image2 = "/images/DEFAULT_IMAGE.jpg",
                         Image3 = "/images/DEFAULT_IMAGE.jpg",
                         Image4 = "/images/DEFAULT_IMAGE.jpg",
                         Image5 = "/images/DEFAULT_IMAGE.jpg",
                         IsActive = 1,
                         UpdatedAt = DateTime.Now
                     },
                     new ProductDetail
                     {
                         Id = 24,
                         ProductId = 24,
                         ProductTypeId = 1,
                         AdditionalInfo = "Define your Size, Color or Anything that occupied space and has mass.",
                         Image1 = "/images/Apple/prod-24.png",
                         Image2 = "/images/DEFAULT_IMAGE.jpg",
                         Image3 = "/images/DEFAULT_IMAGE.jpg",
                         Image4 = "/images/DEFAULT_IMAGE.jpg",
                         Image5 = "/images/DEFAULT_IMAGE.jpg",
                         IsActive = 1,
                         UpdatedAt = DateTime.Now
                     },
                     new ProductDetail
                     {
                         Id = 25,
                         ProductId = 25,
                         ProductTypeId = 1,
                         AdditionalInfo = "Define your Size, Color or Anything that occupied space and has mass.",
                         Image1 = "/images/Apple/prod-25.png",
                         Image2 = "/images/DEFAULT_IMAGE.jpg",
                         Image3 = "/images/DEFAULT_IMAGE.jpg",
                         Image4 = "/images/DEFAULT_IMAGE.jpg",
                         Image5 = "/images/DEFAULT_IMAGE.jpg",
                         IsActive = 1,
                         UpdatedAt = DateTime.Now
                     },
                     new ProductDetail
                     {
                         Id = 26,
                         ProductId = 26,
                         ProductTypeId = 1,
                         AdditionalInfo = "Define your Size, Color or Anything that occupied space and has mass.",
                         Image1 = "/images/Apple/prod-26.png",
                         Image2 = "/images/DEFAULT_IMAGE.jpg",
                         Image3 = "/images/DEFAULT_IMAGE.jpg",
                         Image4 = "/images/DEFAULT_IMAGE.jpg",
                         Image5 = "/images/DEFAULT_IMAGE.jpg",
                         IsActive = 1,
                         UpdatedAt = DateTime.Now
                     },
                     new ProductDetail
                     {
                         Id = 27,
                         ProductId = 27,
                         ProductTypeId = 1,
                         AdditionalInfo = "Define your Size, Color or Anything that occupied space and has mass.",
                         Image1 = "/images/Apple/prod-27.png",
                         Image2 = "/images/DEFAULT_IMAGE.jpg",
                         Image3 = "/images/DEFAULT_IMAGE.jpg",
                         Image4 = "/images/DEFAULT_IMAGE.jpg",
                         Image5 = "/images/DEFAULT_IMAGE.jpg",
                         IsActive = 1,
                         UpdatedAt = DateTime.Now
                     },
                     new ProductDetail
                     {
                         Id = 28,
                         ProductId = 28,
                         ProductTypeId = 1,
                         AdditionalInfo = "Define your Size, Color or Anything that occupied space and has mass.",
                         Image1 = "/images/Apple/prod-28.png",
                         Image2 = "/images/DEFAULT_IMAGE.jpg",
                         Image3 = "/images/DEFAULT_IMAGE.jpg",
                         Image4 = "/images/DEFAULT_IMAGE.jpg",
                         Image5 = "/images/DEFAULT_IMAGE.jpg",
                         IsActive = 1,
                         UpdatedAt = DateTime.Now
                     },
                     new ProductDetail
                     {
                         Id = 29,
                         ProductId = 29,
                         ProductTypeId = 1,
                         AdditionalInfo = "Define your Size, Color or Anything that occupied space and has mass.",
                         Image1 = "/images/Apple/prod-29.png",
                         Image2 = "/images/DEFAULT_IMAGE.jpg",
                         Image3 = "/images/DEFAULT_IMAGE.jpg",
                         Image4 = "/images/DEFAULT_IMAGE.jpg",
                         Image5 = "/images/DEFAULT_IMAGE.jpg",
                         IsActive = 1,
                         UpdatedAt = DateTime.Now
                     },
                     new ProductDetail
                     {
                         Id = 30,
                         ProductId = 30,
                         ProductTypeId = 1,
                         AdditionalInfo = "Define your Size, Color or Anything that occupied space and has mass.",
                         Image1 = "/images/Apple/prod-30.png",
                         Image2 = "/images/DEFAULT_IMAGE.jpg",
                         Image3 = "/images/DEFAULT_IMAGE.jpg",
                         Image4 = "/images/DEFAULT_IMAGE.jpg",
                         Image5 = "/images/DEFAULT_IMAGE.jpg",
                         IsActive = 1,
                         UpdatedAt = DateTime.Now
                     },
                     new ProductDetail
                     {
                         Id = 31,
                         ProductId = 31,
                         ProductTypeId = 1,
                         AdditionalInfo = "Define your Size, Color or Anything that occupied space and has mass.",
                         Image1 = "/images/Realme/prod-31.png",
                         Image2 = "/images/DEFAULT_IMAGE.jpg",
                         Image3 = "/images/DEFAULT_IMAGE.jpg",
                         Image4 = "/images/DEFAULT_IMAGE.jpg",
                         Image5 = "/images/DEFAULT_IMAGE.jpg",
                         IsActive = 1,
                         UpdatedAt = DateTime.Now
                     },
                     new ProductDetail
                     {
                         Id = 32,
                         ProductId = 32,
                         ProductTypeId = 1,
                         AdditionalInfo = "Define your Size, Color or Anything that occupied space and has mass.",
                         Image1 = "/images/Realme/prod-32.png",
                         Image2 = "/images/DEFAULT_IMAGE.jpg",
                         Image3 = "/images/DEFAULT_IMAGE.jpg",
                         Image4 = "/images/DEFAULT_IMAGE.jpg",
                         Image5 = "/images/DEFAULT_IMAGE.jpg",
                         IsActive = 1,
                         UpdatedAt = DateTime.Now
                     },
                     new ProductDetail
                     {
                         Id = 33,
                         ProductId = 33,
                         ProductTypeId = 1,
                         AdditionalInfo = "Define your Size, Color or Anything that occupied space and has mass.",
                         Image1 = "/images/Realme/prod-33.png",
                         Image2 = "/images/DEFAULT_IMAGE.jpg",
                         Image3 = "/images/DEFAULT_IMAGE.jpg",
                         Image4 = "/images/DEFAULT_IMAGE.jpg",
                         Image5 = "/images/DEFAULT_IMAGE.jpg",
                         IsActive = 1,
                         UpdatedAt = DateTime.Now
                     },
                     new ProductDetail
                     {
                         Id = 34,
                         ProductId = 34,
                         ProductTypeId = 1,
                         AdditionalInfo = "Define your Size, Color or Anything that occupied space and has mass.",
                         Image1 = "/images/Realme/prod-34.png",
                         Image2 = "/images/DEFAULT_IMAGE.jpg",
                         Image3 = "/images/DEFAULT_IMAGE.jpg",
                         Image4 = "/images/DEFAULT_IMAGE.jpg",
                         Image5 = "/images/DEFAULT_IMAGE.jpg",
                         IsActive = 1,
                         UpdatedAt = DateTime.Now
                     },
                     new ProductDetail
                     {
                         Id = 35,
                         ProductId = 35,
                         ProductTypeId = 1,
                         AdditionalInfo = "Define your Size, Color or Anything that occupied space and has mass.",
                         Image1 = "/images/Realme/prod-35.png",
                         Image2 = "/images/DEFAULT_IMAGE.jpg",
                         Image3 = "/images/DEFAULT_IMAGE.jpg",
                         Image4 = "/images/DEFAULT_IMAGE.jpg",
                         Image5 = "/images/DEFAULT_IMAGE.jpg",
                         IsActive = 1,
                         UpdatedAt = DateTime.Now
                     },
                     new ProductDetail
                     {
                         Id = 36,
                         ProductId = 36,
                         ProductTypeId = 1,
                         AdditionalInfo = "Define your Size, Color or Anything that occupied space and has mass.",
                         Image1 = "/images/Realme/prod-36.png",
                         Image2 = "/images/DEFAULT_IMAGE.jpg",
                         Image3 = "/images/DEFAULT_IMAGE.jpg",
                         Image4 = "/images/DEFAULT_IMAGE.jpg",
                         Image5 = "/images/DEFAULT_IMAGE.jpg",
                         IsActive = 1,
                         UpdatedAt = DateTime.Now
                     },
                     new ProductDetail
                     {
                         Id = 37,
                         ProductId = 37,
                         ProductTypeId = 1,
                         AdditionalInfo = "Define your Size, Color or Anything that occupied space and has mass.",
                         Image1 = "/images/Realme/prod-37.png",
                         Image2 = "/images/DEFAULT_IMAGE.jpg",
                         Image3 = "/images/DEFAULT_IMAGE.jpg",
                         Image4 = "/images/DEFAULT_IMAGE.jpg",
                         Image5 = "/images/DEFAULT_IMAGE.jpg",
                         IsActive = 1,
                         UpdatedAt = DateTime.Now
                     },
                     new ProductDetail
                     {
                         Id = 38,
                         ProductId = 38,
                         ProductTypeId = 1,
                         AdditionalInfo = "Define your Size, Color or Anything that occupied space and has mass.",
                         Image1 = "/images/Realme/prod-38.png",
                         Image2 = "/images/DEFAULT_IMAGE.jpg",
                         Image3 = "/images/DEFAULT_IMAGE.jpg",
                         Image4 = "/images/DEFAULT_IMAGE.jpg",
                         Image5 = "/images/DEFAULT_IMAGE.jpg",
                         IsActive = 1,
                         UpdatedAt = DateTime.Now
                     },
                     new ProductDetail
                     {
                         Id = 39,
                         ProductId = 39,
                         ProductTypeId = 1,
                         AdditionalInfo = "Define your Size, Color or Anything that occupied space and has mass.",
                         Image1 = "/images/Realme/prod-39.png",
                         Image2 = "/images/DEFAULT_IMAGE.jpg",
                         Image3 = "/images/DEFAULT_IMAGE.jpg",
                         Image4 = "/images/DEFAULT_IMAGE.jpg",
                         Image5 = "/images/DEFAULT_IMAGE.jpg",
                         IsActive = 1,
                         UpdatedAt = DateTime.Now
                     },
                     new ProductDetail
                     {
                         Id = 40,
                         ProductId = 40,
                         ProductTypeId = 1,
                         AdditionalInfo = "Define your Size, Color or Anything that occupied space and has mass.",
                         Image1 = "/images/Realme/prod-40.png",
                         Image2 = "/images/DEFAULT_IMAGE.jpg",
                         Image3 = "/images/DEFAULT_IMAGE.jpg",
                         Image4 = "/images/DEFAULT_IMAGE.jpg",
                         Image5 = "/images/DEFAULT_IMAGE.jpg",
                         IsActive = 1,
                         UpdatedAt = DateTime.Now
                     },
                     new ProductDetail
                     {
                         Id = 41,
                         ProductId = 41,
                         ProductTypeId = 1,
                         AdditionalInfo = "Define your Size, Color or Anything that occupied space and has mass.",
                         Image1 = "/images/Huawei/prod-41.png",
                         Image2 = "/images/DEFAULT_IMAGE.jpg",
                         Image3 = "/images/DEFAULT_IMAGE.jpg",
                         Image4 = "/images/DEFAULT_IMAGE.jpg",
                         Image5 = "/images/DEFAULT_IMAGE.jpg",
                         IsActive = 1,
                         UpdatedAt = DateTime.Now
                     },
                     new ProductDetail
                     {
                         Id = 42,
                         ProductId = 42,
                         ProductTypeId = 1,
                         AdditionalInfo = "Define your Size, Color or Anything that occupied space and has mass.",
                         Image1 = "/images/Huawei/prod-42.png",
                         Image2 = "/images/DEFAULT_IMAGE.jpg",
                         Image3 = "/images/DEFAULT_IMAGE.jpg",
                         Image4 = "/images/DEFAULT_IMAGE.jpg",
                         Image5 = "/images/DEFAULT_IMAGE.jpg",
                         IsActive = 1,
                         UpdatedAt = DateTime.Now
                     },
                     new ProductDetail
                     {
                         Id = 43,
                         ProductId = 43,
                         ProductTypeId = 1,
                         AdditionalInfo = "Define your Size, Color or Anything that occupied space and has mass.",
                         Image1 = "/images/Huawei/prod-43.png",
                         Image2 = "/images/DEFAULT_IMAGE.jpg",
                         Image3 = "/images/DEFAULT_IMAGE.jpg",
                         Image4 = "/images/DEFAULT_IMAGE.jpg",
                         Image5 = "/images/DEFAULT_IMAGE.jpg",
                         IsActive = 1,
                         UpdatedAt = DateTime.Now
                     },
                     new ProductDetail
                     {
                         Id = 44,
                         ProductId = 44,
                         ProductTypeId = 1,
                         AdditionalInfo = "Define your Size, Color or Anything that occupied space and has mass.",
                         Image1 = "/images/Huawei/prod-44.png",
                         Image2 = "/images/DEFAULT_IMAGE.jpg",
                         Image3 = "/images/DEFAULT_IMAGE.jpg",
                         Image4 = "/images/DEFAULT_IMAGE.jpg",
                         Image5 = "/images/DEFAULT_IMAGE.jpg",
                         IsActive = 1,
                         UpdatedAt = DateTime.Now
                     },
                     new ProductDetail
                     {
                         Id = 45,
                         ProductId = 45,
                         ProductTypeId = 1,
                         AdditionalInfo = "Define your Size, Color or Anything that occupied space and has mass.",
                         Image1 = "/images/Huawei/prod-45.png",
                         Image2 = "/images/DEFAULT_IMAGE.jpg",
                         Image3 = "/images/DEFAULT_IMAGE.jpg",
                         Image4 = "/images/DEFAULT_IMAGE.jpg",
                         Image5 = "/images/DEFAULT_IMAGE.jpg",
                         IsActive = 1,
                         UpdatedAt = DateTime.Now
                     },
                     new ProductDetail
                     {
                         Id = 46,
                         ProductId = 46,
                         ProductTypeId = 1,
                         AdditionalInfo = "Define your Size, Color or Anything that occupied space and has mass.",
                         Image1 = "/images/Huawei/prod-46.png",
                         Image2 = "/images/DEFAULT_IMAGE.jpg",
                         Image3 = "/images/DEFAULT_IMAGE.jpg",
                         Image4 = "/images/DEFAULT_IMAGE.jpg",
                         Image5 = "/images/DEFAULT_IMAGE.jpg",
                         IsActive = 1,
                         UpdatedAt = DateTime.Now
                     },
                     new ProductDetail
                     {
                         Id = 47,
                         ProductId = 47,
                         ProductTypeId = 1,
                         AdditionalInfo = "Define your Size, Color or Anything that occupied space and has mass.",
                         Image1 = "/images/Huawei/prod-47.png",
                         Image2 = "/images/DEFAULT_IMAGE.jpg",
                         Image3 = "/images/DEFAULT_IMAGE.jpg",
                         Image4 = "/images/DEFAULT_IMAGE.jpg",
                         Image5 = "/images/DEFAULT_IMAGE.jpg",
                         IsActive = 1,
                         UpdatedAt = DateTime.Now
                     },
                     new ProductDetail
                     {
                         Id = 48,
                         ProductId = 48,
                         ProductTypeId = 1,
                         AdditionalInfo = "Define your Size, Color or Anything that occupied space and has mass.",
                         Image1 = "/images/Huawei/prod-48.png",
                         Image2 = "/images/DEFAULT_IMAGE.jpg",
                         Image3 = "/images/DEFAULT_IMAGE.jpg",
                         Image4 = "/images/DEFAULT_IMAGE.jpg",
                         Image5 = "/images/DEFAULT_IMAGE.jpg",
                         IsActive = 1,
                         UpdatedAt = DateTime.Now
                     },
                     new ProductDetail
                     {
                         Id = 49,
                         ProductId = 49,
                         ProductTypeId = 1,
                         AdditionalInfo = "Define your Size, Color or Anything that occupied space and has mass.",
                         Image1 = "/images/Huawei/prod-49.png",
                         Image2 = "/images/DEFAULT_IMAGE.jpg",
                         Image3 = "/images/DEFAULT_IMAGE.jpg",
                         Image4 = "/images/DEFAULT_IMAGE.jpg",
                         Image5 = "/images/DEFAULT_IMAGE.jpg",
                         IsActive = 1,
                         UpdatedAt = DateTime.Now
                     },
                     new ProductDetail
                     {
                         Id = 50,
                         ProductId = 50,
                         ProductTypeId = 1,
                         AdditionalInfo = "Define your Size, Color or Anything that occupied space and has mass.",
                         Image1 = "/images/Huawei/prod-50.png",
                         Image2 = "/images/DEFAULT_IMAGE.jpg",
                         Image3 = "/images/DEFAULT_IMAGE.jpg",
                         Image4 = "/images/DEFAULT_IMAGE.jpg",
                         Image5 = "/images/DEFAULT_IMAGE.jpg",
                         IsActive = 1,
                         UpdatedAt = DateTime.Now
                     }


                );
            modelBuilder.Entity<ProductType>().HasData(
                new ProductType
                {
                    Id = 1,
                    ProductType1 = "Smartphones",
                    IsActive = 1,
                    UpdatedAt = DateTime.Now

                },
                new ProductType
                {
                    Id = 2,
                    ProductType1 = "Accessories",
                    IsActive = 1,
                    UpdatedAt = DateTime.Now

                }
                );
            modelBuilder.Entity<User>().HasData(
              new User
              {
                  Id = 1000,
                  Address = "10 J.P Rizal st., Brgy. Sta.lucia Novaliches",
                  Role = "CUSTOMER",
                  City = "Quezon City",
                  Contactno = 098609346,
                  Province = "Metro Manila",
                  Zipcode = 1117,
                  Country = "Philippines",
                  Email = "liramarie@hotmail.com",
                  Firstname = "Lira",
                  Lastname = "Marie",
                  Password = "tarantado",
                  Isactive = 1,
                  UpdatedAt = DateTime.Now

              },
              new User
              {
                  Id = 1001,
                  Address = "25 R. Humabon st., Brgy. Sta.lucia Novaliches",
                  Role = "CUSTOMER",
                  City = "Quezon City",
                  Contactno = 098609346,
                  Province = "Metro Manila",
                  Zipcode = 1117,
                  Country = "Philippines",
                  Email = "Michaelangelo1@gmail.com",
                  Firstname = "Michael",
                  Lastname = "Angelo",
                  Password = "09292948752",
                  Isactive = 1,
                  UpdatedAt = DateTime.Now
              },
              new User
              {
                  Id = 1002,
                  Address = "25 G. Lopez Jaena st., Brgy. Sta.lucia Novaliches",
                  Role = "CUSTOMER",
                  City = "Quezon City",
                  Contactno = 098609346,
                  Province = "Metro Manila",
                  Zipcode = 1117,
                  Country = "Philippines",
                  Email = "eot@gmail.com",
                  Firstname = "Jose Mari",
                  Lastname = "Chan",
                  Password = "bermonths123",
                  Isactive = 1,
                  UpdatedAt = DateTime.Now
              },
              new User
              {
                  Id = 1003,
                  Address = "506 J.P Rizal st., Brgy. Sta.lucia Novaliches, Quezon City",
                  Role = "CUSTOMER",
                  City = "Quezon City",
                  Contactno = 098609346,
                  Province = "Metro Manila",
                  Zipcode = 1117,
                  Country = "Philippines",
                  Email = "santosnisarah20@gmail.com",
                  Firstname = "Sarah",
                  Lastname = "Santos",
                  Password = "nabatisarah1",
                  Isactive = 1,
                  UpdatedAt = DateTime.Now
              },
              new User
              {
                  Id = 1004,
                  Address = "11 J. Abad Santos st., Brgy. Sta.lucia Novaliches,Quezon City",
                  Role = "CUSTOMER",
                  City = "Quezon City",
                  Contactno = 098609346,
                  Province = "Metro Manila",
                  Zipcode = 1117,
                  Country = "Philippines",
                  Email = "leasalungga22o@gmail.com",
                  Firstname = "Lea",
                  Lastname = "Salungga",
                  Password = "22osalungga",
                  Isactive = 1,
                  UpdatedAt = DateTime.Now
              },
              new User
              {
                  Id = 1005,
                  Address = "16 E. Aguinaldo st., Brgy. Sta.lucia Novaliches, Quezon City",
                  Role = "CUSTOMER",
                  City = "Quezon City",
                  Contactno = 098609346,
                  Province = "Metro Manila",
                  Zipcode = 1117,
                  Country = "Philippines",
                  Email = "amitafvargas123@gmail.com",
                  Firstname = "Fatima",
                  Lastname = "Vargas",
                  Password = "memyselfandi",
                  Isactive = 1,
                  UpdatedAt = DateTime.Now
              },
              new User
              {
                  Id = 1006,
                  Address = "4 Bago Bantay st., Brgy. Alicia, Quezon City",
                  Role = "CUSTOMER",
                  City = "Quezon City",
                  Contactno = 0932230422,
                  Province = "Metro Manila",
                  Zipcode = 1105,
                  Country = "Philippines",
                  Email = "kengomez5@gmail.com",
                  Firstname = "Ken",
                  Lastname = "Gomez",
                  Password = "akopogiken",
                  Isactive = 1,
                  UpdatedAt = DateTime.Now
              },
              new User
              {
                  Id = 1007,
                  Address = "Road 1, Brgy. Bagong Pag Asa, Quezon City",
                  Role = "CUSTOMER",
                  City = "Quezon City",
                  Contactno = 0965332343,
                  Province = "Metro Manila",
                  Zipcode = 1105,
                  Country = "Philippines",
                  Email = "Jonaldlerum@gmail.com",
                  Firstname = "Jonald",
                  Lastname = "Lerum",
                  Password = "alabyu2",
                  Isactive = 1,
                  UpdatedAt = DateTime.Now
              },
              new User
              {
                  Id = 1008,
                  Address = "27 Pluto st, Brgy. Bahay Toro, Project 8, Quezon City",
                  Role = "CUSTOMER",
                  City = "Quezon City",
                  Contactno = 0943454366,
                  Province = "Metro Manila",
                  Zipcode = 1106,
                  Country = "Philippines",
                  Email = "dickermoran@gmail.com",
                  Firstname = "Mark Dicker",
                  Lastname = "Moran",
                  Password = "matampuhinaq220",
                  Isactive = 1,
                  UpdatedAt = DateTime.Now
              },
              new User
              {
                  Id = 1009,
                  Address = "Biak na bato st, Brgy. Balingasa, Quezon City",
                  Role = "CUSTOMER",
                  City = "Quezon City",
                  Contactno = 0954345224,
                  Province = "Metro Manila",
                  Zipcode = 1115,
                  Country = "Philippines",
                  Email = "rockygolo@gmail.com",
                  Firstname = "Rocky",
                  Lastname = "Golo",
                  Password = "pogitalagaako",
                  Isactive = 1,
                  UpdatedAt = DateTime.Now
              },
              new User
              {
                  Id = 1010,
                  Address = "Ana Maria st, Brgy. Bungad, Quezon City",
                  Role = "CUSTOMER",
                  City = "Quezon City",
                  Contactno = 0923243432,
                  Province = "Metro Manila",
                  Zipcode = 1105,
                  Country = "Philippines",
                  Email = "johnrhodlumabi@gmail.com",
                  Firstname = "John Rhod",
                  Lastname = "Lumabi",
                  Password = "baliwako12",
                  Isactive = 1,
                  UpdatedAt = DateTime.Now
              },
              new User
              {
                  Id = 1011,
                  Address = "132 Sgt.Rivera st, Brgy. Damar, Quezon City",
                  Role = "CUSTOMER",
                  City = "Quezon City",
                  Contactno = 0954234344,
                  Province = "Metro Manila",
                  Zipcode = 1115,
                  Country = "Philippines",
                  Email = "adylegaspi@gmail.com",
                  Firstname = "Ady",
                  Lastname = "Legaspi",
                  Password = "legaspi123",
                  Isactive = 1,
                  UpdatedAt = DateTime.Now
              },
              new User
              {
                  Id = 1012,
                  Address = "22 San Vicente st, Brgy. Damayan, Quezon City",
                  Role = "CUSTOMER",
                  City = "Quezon City",
                  Contactno = 0923423123,
                  Province = "Metro Manila",
                  Zipcode = 1104,
                  Country = "Philippines",
                  Email = "rendelmarcos@gmail.com",
                  Firstname = "rendel",
                  Lastname = "marcos",
                  Password = "renedel12",
                  Isactive = 1,
                  UpdatedAt = DateTime.Now
              },
              new User
              {
                  Id = 1013,
                  Address = "4 Manalo st, Brgy. San Francisco Del Monte, Quezon City",
                  Role = "CUSTOMER",
                  City = "Quezon City",
                  Contactno = 0932343321,
                  Province = "Metro Manila",
                  Zipcode = 1105,
                  Country = "Philippines",
                  Email = "rijdandayanan@gmail.com",
                  Firstname = "Rijelyn",
                  Lastname = "Dandayanan",
                  Password = "dandayananrij45",
                  Isactive = 1,
                  UpdatedAt = DateTime.Now
              },
              new User
              {
                  Id = 1014,
                  Address = "A. Lopez st, Brgy. Katipunan, Quezon City",
                  Role = "CUSTOMER",
                  City = "Quezon City",
                  Contactno = 0986776434,
                  Province = "Metro Manila",
                  Zipcode = 1105,
                  Country = "Philippines",
                  Email = "stefmonel@gmail.com",
                  Firstname = "Steffeny",
                  Lastname = "Monel",
                  Password = "Ilabady123",
                  Isactive = 1,
                  UpdatedAt = DateTime.Now
              },
              new User
              {
                  Id = 1015,
                  Address = "Calamba st, Brgy. Lourdes, Quezon City",
                  Role = "CUSTOMER",
                  City = "Quezon City",
                  Contactno = 0943423232,
                  Province = "Metro Manila",
                  Zipcode = 1114,
                  Country = "Philippines",
                  Email = "danamariel@gmail.com",
                  Firstname = "Dana",
                  Lastname = "Navararro",
                  Password = "danamarie123",
                  Isactive = 1,
                  UpdatedAt = DateTime.Now
              },
              new User
              {
                  Id = 1016,
                  Address = "33 Kanlaon st, Brgy. Maharlika, Quezon City",
                  Role = "CUSTOMER",
                  City = "Quezon City",
                  Contactno = 0954233422,
                  Province = "Metro Manila",
                  Zipcode = 1114,
                  Country = "Philippines",
                  Email = "allenmillabas@gmail.com",
                  Firstname = "Allen",
                  Lastname = "Millabas",
                  Password = "matangkadako12",
                  Isactive = 1,
                  UpdatedAt = DateTime.Now
              },
              new User
              {
                  Id = 1017,
                  Address = "109 Banawe st, Brgy. Manresa, Quezon City",
                  Role = "CUSTOMER",
                  City = "Quezon City",
                  Contactno = 0954339593,
                  Province = "Metro Manila",
                  Zipcode = 1115,
                  Country = "Philippines",
                  Email = "jaspergerero@gmail.com",
                  Firstname = "Jasper",
                  Lastname = "Gerero",
                  Password = "akolange2",
                  Isactive = 1,
                  UpdatedAt = DateTime.Now
              },
              new User
              {
                  Id = 1018,
                  Address = "15 ignacios st, Brgy. Mariblo, Quezon City",
                  Role = "CUSTOMER",
                  City = "Quezon City",
                  Contactno = 0955444323,
                  Province = "Metro Manila",
                  Zipcode = 1104,
                  Country = "Philippines",
                  Email = "joshuacelis@gmail.com",
                  Firstname = "Joshua",
                  Lastname = "Celis",
                  Password = "juswa12345",
                  Isactive = 1,
                  UpdatedAt = DateTime.Now
              },
              new User
              {
                  Id = 1019,
                  Address = "72 Bahawan st, Brgy. Masambong, Quezon City",
                  Role = "CUSTOMER",
                  City = "Quezon City",
                  Contactno = 0943255499,
                  Province = "Metro Manila",
                  Zipcode = 1105,
                  Country = "Philippines",
                  Email = "carlchua@gmail.com",
                  Firstname = "Carl Vincent",
                  Lastname = "Chua",
                  Password = "chuwabels",
                  Isactive = 1,
                  UpdatedAt = DateTime.Now
              },
              new User
              {
                  Id = 1020,
                  Address = "45 Angelo st, Brgy. NS Amoranto , Quezon City",
                  Role = "CUSTOMER",
                  City = "Quezon City",
                  Contactno = 0912332122,
                  Province = "Metro Manila",
                  Zipcode = 1114,
                  Country = "Philippines",
                  Email = "cathymaninang@gmail.com",
                  Firstname = "Cathy",
                  Lastname = "Maninang",
                  Password = "Exkosihoganpogi",
                  Isactive = 1,
                  UpdatedAt = DateTime.Now
              },
              new User
              {
                  Id = 1021,
                  Address = "37 Zamboanga st, Brgy. Nayong Kanluran , Quezon City",
                  Role = "CUSTOMER",
                  City = "Quezon City",
                  Contactno = 0966776809,
                  Province = "Metro Manila",
                  Zipcode = 1104,
                  Country = "Philippines",
                  Email = "clarrisevasquez@gmail.com",
                  Firstname = "Clarisse",
                  Lastname = "Vasquez",
                  Password = "aylabregis24",
                  Isactive = 1,
                  UpdatedAt = DateTime.Now
              },
              new User
              {
                  Id = 1022,
                  Address = "35 Iriga st, Brgy. Paang Bundok , Quezon City",
                  Role = "CUSTOMER",
                  City = "Quezon City",
                  Contactno = 0953320324,
                  Province = "Metro Manila",
                  Zipcode = 1114,
                  Country = "Philippines",
                  Email = "krenzcamacho@gmail.com",
                  Firstname = "Krenz",
                  Lastname = "Camacho",
                  Password = "Mahalkosikuyapat",
                  Isactive = 1,
                  UpdatedAt = DateTime.Now
              },
              new User
              {
                  Id = 1023,
                  Address = "23 Pineda st, Brgy. Pag ibig sa Nayon , Quezon City",
                  Role = "CUSTOMER",
                  City = "Quezon City",
                  Contactno = 0923322323,
                  Province = "Metro Manila",
                  Zipcode = 1115,
                  Country = "Philippines",
                  Email = "markbarte@gmail.com",
                  Firstname = "Mark",
                  Lastname = "Barte",
                  Password = "angprobinsyano45",
                  Isactive = 1,
                  UpdatedAt = DateTime.Now
              },
              new User
              {
                  Id = 1024,
                  Address = "Basa st, Brgy. Paltok , Quezon City",
                  Role = "CUSTOMER",
                  City = "Quezon City",
                  Contactno = 0915643534,
                  Province = "Metro Manila",
                  Zipcode = 1105,
                  Country = "Philippines",
                  Email = "jonnasalo@gmail.com",
                  Firstname = "Jonna mae",
                  Lastname = "Salo",
                  Password = "bebekosihogan66",
                  Isactive = 1,
                  UpdatedAt = DateTime.Now
              },
              new User
              {
                  Id = 1025,
                  Address = "Lot 3232, Brgy. Paraiso , Quezon City",
                  Role = "CUSTOMER",
                  City = "Quezon City",
                  Contactno = 0912332122,
                  Province = "Metro Manila",
                  Zipcode = 1104,
                  Country = "Philippines",
                  Email = "markjorieto@gmail.com",
                  Firstname = "Mark Jorie",
                  Lastname = "Isedera",
                  Password = "baliwako22",
                  Isactive = 1,
                  UpdatedAt = DateTime.Now
              },
              new User
              {
                  Id = 1026,
                  Address = "West Avenue st, Brgy. Phil-Am , Quezon City",
                  Role = "CUSTOMER",
                  City = "Quezon City",
                  Contactno = 0987654322,
                  Province = "Metro Manila",
                  Zipcode = 1104,
                  Country = "Philippines",
                  Email = "jamtejada@gmail.com",
                  Firstname = "Jam",
                  Lastname = "Tejada",
                  Password = "aylabredhorse",
                  Isactive = 1,
                  UpdatedAt = DateTime.Now
              },
              new User
              {
                  Id = 1027,
                  Address = "Road 2 st, Brgy. Project 6 , Quezon City",
                  Role = "CUSTOMER",
                  City = "Quezon City",
                  Contactno = 0912332122,
                  Province = "Metro Manila",
                  Zipcode = 1100,
                  Country = "Philippines",
                  Email = "bogart23@gmail.com",
                  Firstname = "Bogart",
                  Lastname = "Awaw",
                  Password = "akosibogart2",
                  Isactive = 1,
                  UpdatedAt = DateTime.Now
              },
              new User
              {
                  Id = 1028,
                  Address = "Congressional Avenue st, Brgy. Ramon Magsaysay , Quezon City",
                  Role = "CUSTOMER",
                  City = "Quezon City",
                  Contactno = 0964343434,
                  Province = "Metro Manila",
                  Zipcode = 1105,
                  Country = "Philippines",
                  Email = "cathymaninang@gmail.com",
                  Firstname = "Mia",
                  Lastname = "Kalipaparo",
                  Password = "akosimiaganda12",
                  Isactive = 1,
                  UpdatedAt = DateTime.Now
              },
              new User
              {
                  Id = 1029,
                  Address = "98 Bulusan st, Brgy. Salvacion , Quezon City",
                  Role = "CUSTOMER",
                  City = "Quezon City",
                  Contactno = 0934334233,
                  Province = "Metro Manila",
                  Zipcode = 1114,
                  Country = "Philippines",
                  Email = "Cocopogi@gmail.com",
                  Firstname = "Coco",
                  Lastname = "Ricapo",
                  Password = "cocomartin2",
                  Isactive = 1,
                  UpdatedAt = DateTime.Now
              },
              new User
              {
                  Id = 1030,
                  Address = "77 Del Pilar st, Brgy. San Antonio , Quezon City",
                  Role = "CUSTOMER",
                  City = "Quezon City",
                  Contactno = 0918746323,
                  Province = "Metro Manila",
                  Zipcode = 1105,
                  Country = "Philippines",
                  Email = "Tinamorata@gmail.com",
                  Firstname = "Tina",
                  Lastname = "Morata",
                  Password = "tinamoranmorata4",
                  Isactive = 1,
                  UpdatedAt = DateTime.Now
              },
              new User
              {
                  Id = 1031,
                  Address = "Halcon st, Brgy. San Isidro Salvador , Quezon City",
                  Role = "CUSTOMER",
                  City = "Quezon City",
                  Contactno = 0916444353,
                  Province = "Metro Manila",
                  Zipcode = 1113,
                  Country = "Philippines",
                  Email = "bardagul@gmail.com",
                  Firstname = "Bard",
                  Lastname = "Dagul",
                  Password = "pashpash",
                  Isactive = 1,
                  UpdatedAt = DateTime.Now
              },
              new User
              {
                  Id = 1032,
                  Address = "9 Lawis st, Brgy. San Jose , Quezon City",
                  Role = "CUSTOMER",
                  City = "Quezon City",
                  Contactno = 0965544345,
                  Province = "Metro Manila",
                  Zipcode = 1115,
                  Country = "Philippines",
                  Email = "Marktagor5@gmail.com",
                  Firstname = "Mark",
                  Lastname = "Tagor",
                  Password = "Mahalkosiex2",
                  Isactive = 1,
                  UpdatedAt = DateTime.Now
              },
              new User
              {
                  Id = 1033,
                  Address = "8 Sto. Domingo st, Brgy. Sienna , Quezon City",
                  Role = "CUSTOMER",
                  City = "Quezon City",
                  Contactno = 0965434246,
                  Province = "Metro Manila",
                  Zipcode = 1114,
                  Country = "Philippines",
                  Email = "Fatympasandalan@gmail.com",
                  Firstname = "Fatym",
                  Lastname = "Pasandalan",
                  Password = "bhosmaldita4",
                  Isactive = 1,
                  UpdatedAt = DateTime.Now
              },
              new User
              {
                  Id = 1034,
                  Address = "875 Santa Catalina, Brgy. St. Peter , Quezon City",
                  Role = "CUSTOMER",
                  City = "Quezon City",
                  Contactno = 0966545222,
                  Province = "Metro Manila",
                  Zipcode = 1114,
                  Country = "Philippines",
                  Email = "Jammagno@gmail.com",
                  Firstname = "Jam",
                  Lastname = "Magno",
                  Password = "Barbielat56",
                  Isactive = 1,
                  UpdatedAt = DateTime.Now
              },
              new User
              {
                  Id = 1035,
                  Address = "P.Florentino st, Brgy. Santa Cruz , Quezon City",
                  Role = "CUSTOMER",
                  City = "Quezon City",
                  Contactno = 0965355234,
                  Province = "Metro Manila",
                  Zipcode = 1104,
                  Country = "Philippines",
                  Email = "Pedropenduko@gmail.com",
                  Firstname = "Pedto",
                  Lastname = "Penduko",
                  Password = "agimat22o",
                  Isactive = 1,
                  UpdatedAt = DateTime.Now
              },
              new User
              {
                  Id = 1036,
                  Address = "11 Mayon st, Brgy. Sta. Teresita , Quezon City",
                  Role = "CUSTOMER",
                  City = "Quezon City",
                  Contactno = 0963454233,
                  Province = "Metro Manila",
                  Zipcode = 1114,
                  Country = "Philippines",
                  Email = "JessicaSuho@gmail.com",
                  Firstname = "Jessica",
                  Lastname = "Suho",
                  Password = "memberakongexo",
                  Isactive = 1,
                  UpdatedAt = DateTime.Now
              },
              new User
              {
                  Id = 1037,
                  Address = "753 Romblon st, Brgy. Sto. Cristo , Quezon City",
                  Role = "CUSTOMER",
                  City = "Quezon City",
                  Contactno = 0965430988,
                  Province = "Metro Manila",
                  Zipcode = 1105,
                  Country = "Philippines",
                  Email = "GlenBitiis@gmail.com",
                  Firstname = "Glen",
                  Lastname = "Bitiis",
                  Password = "bitiisbiot",
                  Isactive = 1,
                  UpdatedAt = DateTime.Now
              },
              new User
              {
                  Id = 1038,
                  Address = "09 Sarsa st, Brgy. Sto. Domingo , Quezon City",
                  Role = "CUSTOMER",
                  City = "Quezon City",
                  Contactno = 0966533323,
                  Province = "Metro Manila",
                  Zipcode = 1114,
                  Country = "Philippines",
                  Email = "Kennethregis@gmail.com",
                  Firstname = "Kenneth",
                  Lastname = "Regis",
                  Password = "aylabklaris",
                  Isactive = 1,
                  UpdatedAt = DateTime.Now
              },
              new User
              {
                  Id = 1039,
                  Address = "099 Calamba st, Brgy. Talayan , Quezon City",
                  Role = "CUSTOMER",
                  City = "Quezon City",
                  Contactno = 0987777655,
                  Province = "Metro Manila",
                  Zipcode = 1104,
                  Country = "Philippines",
                  Email = "JadeOloroso@gmail.com",
                  Firstname = "Jade",
                  Lastname = "Oloroso",
                  Password = "Mahalkojowaq12",
                  Isactive = 1,
                  UpdatedAt = DateTime.Now
              },
              new User
              {
                  Id = 1040,
                  Address = "659 Carmen st, Brgy. Vasra , Quezon City",
                  Role = "CUSTOMER",
                  City = "Quezon City",
                  Contactno = 0965544345,
                  Province = "Metro Manila",
                  Zipcode = 1128,
                  Country = "Philippines",
                  Email = "Emmansuelto@gmail.com",
                  Firstname = "Emman",
                  Lastname = "Suelto",
                  Password = "pogipogiko",
                  Isactive = 1,
                  UpdatedAt = DateTime.Now
              },
              new User
              {
                  Id = 1041,
                  Address = "438 Balangkas st, Brgy. Arkong bato , Valenzuela City",
                  Role = "CUSTOMER",
                  City = "Valenzuela City",
                  Contactno = 0907654445,
                  Province = "Metro Manila",
                  Zipcode = 1444,
                  Country = "Philippines",
                  Email = "jasongallera@gmail.com",
                  Firstname = "jason",
                  Lastname = "gallera",
                  Password = "bhosmapagmahal02",
                  Isactive = 1,
                  UpdatedAt = DateTime.Now
              },
              new User
              {
                  Id = 1042,
                  Address = "3 Green Meadows st, Brgy. Lawang Bato , Valenzuela City",
                  Role = "CUSTOMER",
                  City = "Valenzuela City",
                  Contactno = 0998675452,
                  Province = "Metro Manila",
                  Zipcode = 1447,
                  Country = "Philippines",
                  Email = "venjellouolivare@gmail.com",
                  Firstname = "Venjellou",
                  Lastname = "Olivare",
                  Password = "memerako",
                  Isactive = 1,
                  UpdatedAt = DateTime.Now
              },
              new User
              {
                  Id = 1043,
                  Address = "5 Marton rd, Brgy. Canumay East , Valenzuela City",
                  Role = "CUSTOMER",
                  City = "Valenzuela City",
                  Contactno = 0966656778,
                  Province = "Metro Manila",
                  Zipcode = 1447,
                  Country = "Philippines",
                  Email = "jewellmayrena@gmail.com",
                  Firstname = "Jewell",
                  Lastname = "Mayrena",
                  Password = "bhozmakulit67",
                  Isactive = 1,
                  UpdatedAt = DateTime.Now
              },
              new User
              {
                  Id = 1044,
                  Address = "8 Chateu Condiminium, Brgy. Canumay West, Valenzuela City",
                  Role = "CUSTOMER",
                  City = "Valenzuela City",
                  Contactno = 0968565543,
                  Province = "Metro Manila",
                  Zipcode = 1443,
                  Country = "Philippines",
                  Email = "Richardco@gmail.com",
                  Firstname = "Richard",
                  Lastname = "Co",
                  Password = "tsingtsong2",
                  Isactive = 1,
                  UpdatedAt = DateTime.Now
              },
              new User
              {
                  Id = 1045,
                  Address = "De Jesus st, Brgy. Gen. T Deleon , Valenzuela City",
                  Role = "CUSTOMER",
                  City = "Valenzuela City",
                  Contactno = 0962335435,
                  Province = "Metro Manila",
                  Zipcode = 1442,
                  Country = "Philippines",
                  Email = " Jovilyncamaya@gmail.com",
                  Firstname = "Jovilyn",
                  Lastname = "Camaya",
                  Password = "jubilen23",
                  Isactive = 1,
                  UpdatedAt = DateTime.Now
              },
              new User
              {
                  Id = 1046,
                  Address = "Langit Rd, Brgy. Bagong Silang , Caloocan City",
                  Role = "CUSTOMER",
                  City = "Caloocan City",
                  Contactno = 0954434255,
                  Province = "Metro Manila",
                  Zipcode = 1428,
                  Country = "Philippines",
                  Email = "Jemammen@gmail.com",
                  Firstname = "Jem",
                  Lastname = "Ammen",
                  Password = "onlineselleraq",
                  Isactive = 1,
                  UpdatedAt = DateTime.Now
              },
              new User
              {
                  Id = 1047,
                  Address = "12 Unang Sigaw, Brgy. Bagong Barrio West , Caloocan City",
                  Role = "CUSTOMER",
                  City = "Caloocan City",
                  Contactno = 0934343224,
                  Province = "Metro Manila",
                  Zipcode = 1421,
                  Country = "Philippines",
                  Email = "Adraindelrosario@gmail.com",
                  Firstname = "Adrian",
                  Lastname = "Del Rosario",
                  Password = "bhosztangkad",
                  Isactive = 1,
                  UpdatedAt = DateTime.Now
              },
              new User
              {
                  Id = 1048,
                  Address = "Gremville Subdivision, Brgy. Bagbaguin , Caloocan City",
                  Role = "CUSTOMER",
                  City = "Caloocan City",
                  Contactno = 0964345446,
                  Province = "Metro Manila",
                  Zipcode = 1440,
                  Country = "Philippines",
                  Email = "arielcalipay@gmail.com",
                  Firstname = "Ariel",
                  Lastname = "Calipay",
                  Password = "boijackpot3",
                  Isactive = 1,
                  UpdatedAt = DateTime.Now
              },
              new User
              {
                  Id = 1049,
                  Address = "7b Belfast st, Brgy. Kaybiga , Caloocan City",
                  Role = "CUSTOMER",
                  City = "Caloocan City",
                  Contactno = 0954542442,
                  Province = "Metro Manila",
                  Zipcode = 1420,
                  Country = "Philippines",
                  Email = "Allenmabaling@gmail.com",
                  Firstname = "Allen",
                  Lastname = "Mabaling",
                  Password = "allentangkad67",
                  Isactive = 1,
                  UpdatedAt = DateTime.Now
              },
              new User
              {
                  Id = 1050,
                  Address = "89 Jade st, Brgy. Deparo , Caloocan City",
                  Role = "CUSTOMER",
                  City = "Caloocan City",
                  Contactno = 0954543450,
                  Province = "Metro Manila",
                  Zipcode = 1420,
                  Country = "Philippines",
                  Email = "Carlvincenttabin@gmail.com",
                  Firstname = "Carl Vincent",
                  Lastname = "Tabin",
                  Password = "boijollibee",
                  Isactive = 1,
                  UpdatedAt = DateTime.Now
              }

              );

            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<Order>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Courier).IsUnicode(false);

                entity.Property(e => e.Status).IsUnicode(false);

                entity.Property(e => e.TrackingNo).IsUnicode(false);

                entity.HasOne(d => d.ProductDetails)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.ProductDetailsId)
                    .HasConstraintName("FK_orders_product_details");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.ProductId)
                    .HasConstraintName("FK_orders_products");

                entity.HasOne(d => d.ProductType)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.ProductTypeId)
                    .HasConstraintName("FK_orders_product_type");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_orders_users");
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.ProductCode).IsUnicode(false);

                entity.Property(e => e.ProductDescription).IsUnicode(false);

                entity.Property(e => e.ProductName).IsUnicode(false);
            });

            modelBuilder.Entity<ProductDetail>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.AdditionalInfo).IsUnicode(false);

                entity.Property(e => e.Image1).IsUnicode(false);

                entity.Property(e => e.Image2).IsUnicode(false);

                entity.Property(e => e.Image3).IsUnicode(false);

                entity.Property(e => e.Image4).IsUnicode(false);

                entity.Property(e => e.Image5).IsUnicode(false);

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ProductDetails)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_product_details_products");

                entity.HasOne(d => d.ProductType)
                    .WithMany(p => p.ProductDetails)
                    .HasForeignKey(d => d.ProductTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_product_details_product_type");
            });

            modelBuilder.Entity<ProductType>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.ProductType1).IsUnicode(false);
            });

            modelBuilder.Entity<ShoppingCart>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.HasOne(d => d.ProductDetails)
                    .WithMany(p => p.ShoppingCarts)
                    .HasForeignKey(d => d.ProductDetailsId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_shopping_cart_product_details");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ShoppingCarts)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_shopping_cart_products");

                entity.HasOne(d => d.ProductType)
                    .WithMany(p => p.ShoppingCarts)
                    .HasForeignKey(d => d.ProductTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_shopping_cart_product_type");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.ShoppingCarts)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_shopping_cart_users");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Address).IsUnicode(false);

                entity.Property(e => e.City).IsUnicode(false);

                entity.Property(e => e.Country).IsUnicode(false);

                entity.Property(e => e.Email).IsUnicode(false);

                entity.Property(e => e.Firstname).IsUnicode(false);

                entity.Property(e => e.Lastname).IsUnicode(false);

                entity.Property(e => e.Password).IsUnicode(false);

                entity.Property(e => e.Province).IsUnicode(false);

                entity.Property(e => e.Role).IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
