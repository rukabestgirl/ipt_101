﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace IPT_101.Repository.Models
{
    [Table("product_details")]
    [Index(nameof(ProductId), Name = "IX_product_details_PRODUCT_ID")]
    [Index(nameof(ProductTypeId), Name = "IX_product_details_PRODUCT_TYPE_ID")]
    public partial class ProductDetail
    {
        public ProductDetail()
        {
            Orders = new HashSet<Order>();
            ShoppingCarts = new HashSet<ShoppingCart>();
        }

        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Column("PRODUCT_ID")]
        public int ProductId { get; set; }
        [Column("PRODUCT_TYPE_ID")]
        public int ProductTypeId { get; set; }
        [Required]
        [Column("ADDITIONAL_INFO")]
        public string AdditionalInfo { get; set; }
        [Required]
        [Column("IMAGE1")]
        public string Image1 { get; set; }
        [Required]
        [Column("IMAGE2")]
        public string Image2 { get; set; }
        [Required]
        [Column("IMAGE3")]
        public string Image3 { get; set; }
        [Required]
        [Column("IMAGE4")]
        public string Image4 { get; set; }
        [Required]
        [Column("IMAGE5")]
        public string Image5 { get; set; }
        [Column("IS_ACTIVE")]
        public short IsActive { get; set; }
        [Column("UPDATED_AT")]
        public DateTime UpdatedAt { get; set; }

        [ForeignKey(nameof(ProductId))]
        [InverseProperty("ProductDetails")]
        public virtual Product Product { get; set; }
        [ForeignKey(nameof(ProductTypeId))]
        [InverseProperty("ProductDetails")]
        public virtual ProductType ProductType { get; set; }
        [InverseProperty(nameof(Order.ProductDetails))]
        public virtual ICollection<Order> Orders { get; set; }
        [InverseProperty(nameof(ShoppingCart.ProductDetails))]
        public virtual ICollection<ShoppingCart> ShoppingCarts { get; set; }
    }
}
