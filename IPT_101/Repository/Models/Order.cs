﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace IPT_101.Repository.Models
{
    [Table("orders")]
    public partial class Order
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Column("PRODUCT_ID")]
        public int? ProductId { get; set; }
        [Column("PRODUCT_TYPE_ID")]
        public int? ProductTypeId { get; set; }
        [Column("PRODUCT_DETAILS_ID")]
        public int? ProductDetailsId { get; set; }
        [Column("USER_ID")]
        public int? UserId { get; set; }
        [Column("QUANTITY")]
        public int? Quantity { get; set; }
        [Column("PREPARATION_IN_DAYS")]
        public int? PreparationInDays { get; set; }
        [Column("SHIPMENT_IN_DAYS")]
        public int? ShipmentInDays { get; set; }
        [Column("COURIER")]
        public string Courier { get; set; }
        [Column("TRACKING_NO")]
        public string TrackingNo { get; set; }
        [Column("TOTAL")]
        public int? Total { get; set; }
        [Column("STATUS")]
        public string Status { get; set; }

        [ForeignKey(nameof(ProductId))]
        [InverseProperty("Orders")]
        public virtual Product Product { get; set; }
        [ForeignKey(nameof(ProductDetailsId))]
        [InverseProperty(nameof(ProductDetail.Orders))]
        public virtual ProductDetail ProductDetails { get; set; }
        [ForeignKey(nameof(ProductTypeId))]
        [InverseProperty("Orders")]
        public virtual ProductType ProductType { get; set; }
        [ForeignKey(nameof(UserId))]
        [InverseProperty("Orders")]
        public virtual User User { get; set; }
    }
}
