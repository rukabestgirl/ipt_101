﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace IPT_101.Repository.Models
{
    [Table("users")]
    public partial class User
    {
        public User()
        {
            Orders = new HashSet<Order>();
            ShoppingCarts = new HashSet<ShoppingCart>();
        }

        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Required]
        [Column("ROLE")]
        [StringLength(50)]
        public string Role { get; set; }
        [Required]
        [Column("FIRSTNAME")]
        [StringLength(50)]
        public string Firstname { get; set; }
        [Required]
        [Column("LASTNAME")]
        [StringLength(50)]
        public string Lastname { get; set; }
        [Column("CONTACTNO")]
        public int Contactno { get; set; }
        [Required]
        [Column("ADDRESS")]
        public string Address { get; set; }
        [Required]
        [Column("CITY")]
        [StringLength(50)]
        public string City { get; set; }
        [Required]
        [Column("PROVINCE")]
        [StringLength(50)]
        public string Province { get; set; }
        [Column("ZIPCODE")]
        public int Zipcode { get; set; }
        [Required]
        [Column("COUNTRY")]
        [StringLength(50)]
        public string Country { get; set; }
        [Required]
        [Column("EMAIL")]
        public string Email { get; set; }
        [Required]
        [Column("PASSWORD")]
        [StringLength(50)]
        public string Password { get; set; }
        [Column("ISACTIVE")]
        public short Isactive { get; set; }
        [Column("UPDATED_AT")]
        public DateTime UpdatedAt { get; set; }

        [InverseProperty(nameof(Order.User))]
        public virtual ICollection<Order> Orders { get; set; }
        [InverseProperty(nameof(ShoppingCart.User))]
        public virtual ICollection<ShoppingCart> ShoppingCarts { get; set; }
    }
}
