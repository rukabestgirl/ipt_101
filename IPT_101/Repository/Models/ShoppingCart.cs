﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace IPT_101.Repository.Models
{
    [Table("shopping_cart")]
    [Index(nameof(ProductDetailsId), Name = "IX_shopping_cart_PRODUCT_DETAILS_ID")]
    [Index(nameof(ProductId), Name = "IX_shopping_cart_PRODUCT_ID")]
    [Index(nameof(ProductTypeId), Name = "IX_shopping_cart_PRODUCT_TYPE_ID")]
    [Index(nameof(UserId), Name = "IX_shopping_cart_USER_ID")]
    public partial class ShoppingCart
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Column("PRODUCT_ID")]
        public int ProductId { get; set; }
        [Column("PRODUCT_TYPE_ID")]
        public int ProductTypeId { get; set; }
        [Column("PRODUCT_DETAILS_ID")]
        public int ProductDetailsId { get; set; }
        [Column("USER_ID")]
        public int UserId { get; set; }
        [Column("UNIT_PRICE")]
        public int UnitPrice { get; set; }
        [Column("TOTAL_PRICE")]
        public int TotalPrice { get; set; }
        [Column("QUANTITY")]
        public int Quantity { get; set; }

        [ForeignKey(nameof(ProductId))]
        [InverseProperty("ShoppingCarts")]
        public virtual Product Product { get; set; }
        [ForeignKey(nameof(ProductDetailsId))]
        [InverseProperty(nameof(ProductDetail.ShoppingCarts))]
        public virtual ProductDetail ProductDetails { get; set; }
        [ForeignKey(nameof(ProductTypeId))]
        [InverseProperty("ShoppingCarts")]
        public virtual ProductType ProductType { get; set; }
        [ForeignKey(nameof(UserId))]
        [InverseProperty("ShoppingCarts")]
        public virtual User User { get; set; }
    }
}
