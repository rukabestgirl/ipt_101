﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace IPT_101.Repository.Models
{
    [Table("product_type")]
    public partial class ProductType
    {
        public ProductType()
        {
            Orders = new HashSet<Order>();
            ProductDetails = new HashSet<ProductDetail>();
            ShoppingCarts = new HashSet<ShoppingCart>();
        }

        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Required]
        [Column("PRODUCT_TYPE")]
        public string ProductType1 { get; set; }
        [Column("IS_ACTIVE")]
        public short IsActive { get; set; }
        [Column("UPDATED_AT")]
        public DateTime UpdatedAt { get; set; }

        [InverseProperty(nameof(Order.ProductType))]
        public virtual ICollection<Order> Orders { get; set; }
        [InverseProperty(nameof(ProductDetail.ProductType))]
        public virtual ICollection<ProductDetail> ProductDetails { get; set; }
        [InverseProperty(nameof(ShoppingCart.ProductType))]
        public virtual ICollection<ShoppingCart> ShoppingCarts { get; set; }
    }
}
