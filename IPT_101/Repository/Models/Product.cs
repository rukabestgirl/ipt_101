﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace IPT_101.Repository.Models
{
    [Table("products")]
    public partial class Product
    {
        public Product()
        {
            Orders = new HashSet<Order>();
            ProductDetails = new HashSet<ProductDetail>();
            ShoppingCarts = new HashSet<ShoppingCart>();
        }

        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Required]
        [Column("PRODUCT_CODE")]
        [StringLength(50)]
        public string ProductCode { get; set; }
        [Required]
        [Column("PRODUCT_NAME")]
        [StringLength(50)]
        public string ProductName { get; set; }
        [Required]
        [Column("PRODUCT_DESCRIPTION")]
        [StringLength(50)]
        public string ProductDescription { get; set; }
        [Column("SUGGESTED_UNIT_PRICE")]
        public int SuggestedUnitPrice { get; set; }
        [Column("UNITS_IN_STOCK")]
        public int UnitsInStock { get; set; }
        [Column("PARCEL_PREPARATION_IN_DAYS")]
        public int ParcelPreparationInDays { get; set; }
        [Column("IS_ACTIVE")]
        public short IsActive { get; set; }

        [InverseProperty(nameof(Order.Product))]
        public virtual ICollection<Order> Orders { get; set; }
        [InverseProperty(nameof(ProductDetail.Product))]
        public virtual ICollection<ProductDetail> ProductDetails { get; set; }
        [InverseProperty(nameof(ShoppingCart.Product))]
        public virtual ICollection<ShoppingCart> ShoppingCarts { get; set; }
    }
}
