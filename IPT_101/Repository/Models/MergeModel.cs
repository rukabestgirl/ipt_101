﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IPT_101.Repository.Models
{
    public class MergeModel
    {
        public Product Products { get; set; }
        public ProductDetail ProductDetails { get; set; }
        public ProductType ProductType { get; set; }
    }
}
