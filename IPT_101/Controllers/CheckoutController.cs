﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IPT_101.Repository;
using IPT_101.Repository.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace IPT_101.Controllers
{
    public class CheckoutController : Controller
    {
        private readonly IPT_101Context _context;

        public CheckoutController(IPT_101Context context)
        {
            _context = context;
        }
        // GET: CheckoutController
        public async Task<ActionResult> IndexAsync(int UserId)
        {
            var iPT_101Context = _context.ShoppingCarts.Include(s => s.Product).Include(s => s.ProductDetails).Include(s => s.ProductType).Include(s => s.User).Where(s => s.UserId == UserId);
            return View(await iPT_101Context.ToListAsync());
        }

        [HttpPost]
        public async Task<PartialViewResult> CheckoutAsync(int Id)
        {
            List<ShoppingCart> sc = new List<ShoppingCart>();
            sc = await (from ShoppingCart in _context.ShoppingCarts
                        .Include(s => s.Product)
                        .Include(s => s.ProductDetails)
                        .Include(s => s.ProductType)
                        .Include(s => s.User)
                        .Where(p => p.UserId == Id)
                        select ShoppingCart).ToListAsync();
            var result = await _context.ShoppingCarts
                             .Include(s => s.Product)
                             .Include(s => s.ProductDetails)
                             .Include(s => s.ProductType)
                             .Include(s => s.User)
                             .Where(p => p.UserId == Id)
                             .FirstOrDefaultAsync();

            var ser = await _context.ShoppingCarts.Where(p => p.UserId == Id).ToListAsync();
            //ViewBag.CartCount = ser.Count();
            //ViewBag.FirstName = result.User.Firstname;
            //ViewBag.Lastname = result.User.Lastname;
            //ViewBag.Address = result.User.Address;
            //ViewBag.City = result.User.City;
            //ViewBag.Province = result.User.Province;
            //ViewBag.Country = result.User.Country;
            //ViewBag.Lastname = result.User.Lastname;
            return PartialView("_Checkout", sc);
        }
        // GET: CheckoutController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: CheckoutController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CheckoutController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(IndexAsync));
            }
            catch
            {
                return View();
            }
        }

        // GET: CheckoutController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: CheckoutController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(IndexAsync));
            }
            catch
            {
                return View();
            }
        }

        // GET: CheckoutController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: CheckoutController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(IndexAsync));
            }
            catch
            {
                return View();
            }
        }
    }
}
