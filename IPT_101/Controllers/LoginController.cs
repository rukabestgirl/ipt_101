﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IPT_101.Models;
using IPT_101.Repository;
using IPT_101.Repository.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace IPT_101.Controllers
{
    public class LoginController : Controller
    {
        private readonly IPT_101Context _context;

        public LoginController(IPT_101Context context)
        {
            _context = context;
        }

        db dbop = new db();
        // GET: LoginController
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> IndexAsync([Bind] Login login)
        {
            var user = await _context.Users
                .FirstOrDefaultAsync(m => m.Email == login.Email);
            int res = dbop.LogInCheck(login);
            if (res == 1)
            {
                TempData["msg"] = "Welcome customer";
                ViewBag.User = user;
                return View(user);

                //return RedirectToAction("Index", "Myaccount");
            }
            else
            {
                TempData["msg"] = "Email or Password is wrong!";
            }


            return View();
        }


        // GET: LoginController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: LoginController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: LoginController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: LoginController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: LoginController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: LoginController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: LoginController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
