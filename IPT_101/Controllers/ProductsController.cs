﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using IPT_101.Repository;
using IPT_101.Repository.Models;
using cloudscribe.Pagination.Models;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace IPT_101.Controllers
{

    public class ProductsController : Controller
    {
        private readonly IPT_101Context _context;

        public ProductsController(IPT_101Context context)
        {
           var result = _context = context;
        }

        public PartialViewResult SearchProducts(string searchText, int pageNumber = 1, int pageSize = 10)
        {
           
            int ExcludeRecords = (pageSize * pageNumber) - pageSize;
            var iPT_101Context = from b in _context.ProductDetails.Include(p => p.Product).Include(p => p.ProductType)
                                 select b;
            if (!String.IsNullOrEmpty(searchText))
            {
                iPT_101Context = iPT_101Context.Where(b => b.Product.ProductName.Contains(searchText));
            }
            
            iPT_101Context = iPT_101Context = iPT_101Context
                .Skip(ExcludeRecords)
                .Take(pageSize);
            return PartialView("_GridView", iPT_101Context);
        }
        // GET: Products
        public IActionResult Index2()
        {
            //var iPT_101Context = _context.ProductDetails.Include(p => p.Product).Include(p => p.ProductType);
            return View();
        }
        
        public async Task<IActionResult> Index(string searchString,string sortOrder,int pageNumber = 1, int pageSize = 10)
        {
            List<ProductType> productTypes = new List<ProductType>();
            productTypes = (from ProductType in _context.ProductTypes
                            select ProductType).ToList();

            ViewBag.ListOfCategory = productTypes;
            ViewBag.CurrentSortOrder = sortOrder;
            ViewBag.DefaultSortParam = "";
            ViewBag.PriceDescSortParam ="price_desc" ;
            ViewBag.PriceAscSortParam = "price_asc" ;
            ViewBag.TotalItems = _context.ProductDetails.Count();
            ViewBag.PageNumber = pageNumber;
            ViewBag.PageSize = pageSize;
            int ExcludeRecords = (pageSize * pageNumber) - pageSize;
            //String.IsNullOrEmpty(sortOrder) ? "price_desc": "";
            var iPT_101Context = from b in _context.ProductDetails.Include(p => p.Product).Include(p => p.ProductType)
                                 select b;

            //var shoppingcart = await _context.ShoppingCarts
                //.Include(p => p.Product)
                //.Include(p => p.User)
                //.FirstOrDefaultAsync(m => m.Id == id);
                

            //Sorting Logic
            switch (sortOrder)
            {
                case "price_desc":
                    iPT_101Context = iPT_101Context.OrderByDescending(b => b.Product.SuggestedUnitPrice);
                    break;
                case "price_asc":
                    iPT_101Context = iPT_101Context.OrderBy(b => b.Product.SuggestedUnitPrice);
                    break;
                default:
                    iPT_101Context = _context.ProductDetails.Include(p => p.Product).Include(p => p.ProductType);
                    break;
            }

            if (!String.IsNullOrEmpty(searchString))
            {
                iPT_101Context = iPT_101Context.Where(b => b.Product.ProductName.Contains(searchString));
            }
            
            iPT_101Context = iPT_101Context
                .Skip(ExcludeRecords)
                .Take(pageSize);
            
            var result = new PagedResult<ProductDetail>
            {
                Data = iPT_101Context.AsNoTracking().ToList(),
                TotalItems = _context.ProductDetails.Count(),
                PageNumber = pageNumber,
                PageSize = pageSize
                

            };
            
            var options = new JsonSerializerOptions()
            {
                ReferenceHandler = ReferenceHandler.Preserve
            };
            var objstr = JsonSerializer.Serialize(result, options);
            ViewBag.ListOfProducts = objstr;

            ViewBag.Data = iPT_101Context.AsNoTracking().ToList();

            //ViewBag.ShoppingCart = shoppingcart;
            return View(result);
        }
        [HttpPost]
         //need tanggalin to 
        public async Task<IActionResult> Index(int ProductId, int ProductTypeId, int UserId, int UnitPrice, int TotalPrice, int Quantity, ShoppingCart shoppingCart, int id = 0)
        {
            //may mali pa kapag nag start tayo uli ng bagong session di dumadagdag sa latest cart

            var ser = await _context.ShoppingCarts.OrderByDescending(c => c.Id).FirstOrDefaultAsync();

            if (ModelState.IsValid)
            {
                var result = new ShoppingCart();

                if(ser == null || shoppingCart == null)
                {
                    result.Id = 0;
                    result.ProductDetailsId = ProductId;
                    result.ProductId = ProductId;
                    result.UserId = UserId;
                    result.ProductTypeId = ProductTypeId;
                    result.UnitPrice = UnitPrice;
                    result.TotalPrice = TotalPrice;
                    result.Quantity = Quantity;
                    _context.Add(result);
                    await _context.SaveChangesAsync();
                    
                    ViewData["cartcount"] = _context.ShoppingCarts.Count();
                    ViewBag.CartCount = ViewData["cartcount"];
                    return RedirectToAction(nameof(Index));
                }
                else if(ser != null || shoppingCart != null)
                {
                    shoppingCart = (ShoppingCart) await _context.ShoppingCarts.Where(predicate: p => p.UserId == UserId).FirstOrDefaultAsync(p => p.ProductId == ProductId);

                    if (ser.UserId == UserId && ser.ProductId == ProductId)
                    {

                        
                        //var ito = shop.ShoppingCarts.Find(UserId);
                        //null daw si ito.totalprice
                        //ito.TotalPrice = TotalPrice + TotalPrice;
                        //shop.Entry(ito).CurrentValues.SetValues(TotalPrice);
                        shoppingCart.Id = shoppingCart.Id;
                        shoppingCart.ProductDetailsId = ProductId;
                        shoppingCart.ProductId = ProductId;
                        shoppingCart.UserId = UserId;
                        shoppingCart.ProductTypeId = ProductTypeId;
                        shoppingCart.UnitPrice = UnitPrice;
                        shoppingCart.TotalPrice = shoppingCart.TotalPrice + TotalPrice;
                        shoppingCart.Quantity = shoppingCart.Quantity + Quantity;
                        
                        await _context.SaveChangesAsync();
                        ViewData["cartcount"] = _context.ShoppingCarts.Count();
                        ViewBag.CartCount = ViewData["cartcount"];
                        return RedirectToAction(nameof(Index));
                        //_context.Entry(shoppingCart).State = EntityState.Unchanged;
                        //_context.Attach(shoppingCart);
                        //shoppingCart.TotalPrice = TotalPrice + TotalPrice;

                    }else if (shoppingCart != null)
                    {
                        shoppingCart.Id = shoppingCart.Id;
                        shoppingCart.ProductDetailsId = ProductId;
                        shoppingCart.ProductId = ProductId;
                        shoppingCart.UserId = UserId;
                        shoppingCart.ProductTypeId = ProductTypeId;
                        shoppingCart.UnitPrice = UnitPrice;
                        shoppingCart.TotalPrice = shoppingCart.TotalPrice + TotalPrice;
                        shoppingCart.Quantity = shoppingCart.Quantity + Quantity;
                        await _context.SaveChangesAsync();
                        ViewData["cartcount"] = _context.ShoppingCarts.Count();
                        ViewBag.CartCount = ViewData["cartcount"];
                        return RedirectToAction(nameof(Index));
                    }
                    else
                    {
                        result.Id = ser.Id + 1;
                        result.ProductDetailsId = ProductId;
                        result.ProductId = ProductId;
                        result.UserId = UserId;
                        result.ProductTypeId = ProductTypeId;
                        result.UnitPrice = UnitPrice;
                        result.TotalPrice = TotalPrice;
                        result.Quantity = Quantity;
                        _context.Add(result);
                        await _context.SaveChangesAsync();
                        ViewData["cartcount"] = _context.ShoppingCarts.Count();
                        ViewBag.CartCount = ViewData["cartcount"];
                        return RedirectToAction(nameof(Index));
                    }
                }

                
            }

            return RedirectToAction(nameof(Index));
            //return View(shoppingCart);
        }

        // GET: Products/Details/5
        [HttpPost]
        public async Task<IActionResult> AddToCart(int Id,int ProductId, int ProductTypeId, int UserId, int UnitPrice, int TotalPrice, int Quantity, ShoppingCart shoppingCart, int id = 0)
        {
            //may mali pa kapag nag start tayo uli ng bagong session di dumadagdag sa latest cart
            var productDetail = await _context.ProductDetails
                .Include(p => p.Product)
                .Include(p => p.ProductType)
                .FirstOrDefaultAsync(m => m.Id == Id);
            var ser = await _context.ShoppingCarts.OrderByDescending(c => c.Id).FirstOrDefaultAsync();

            if (ModelState.IsValid)
            {
                var result = new ShoppingCart();

                if (ser == null || shoppingCart == null)
                {
                    result.Id = 0;
                    result.ProductDetailsId = ProductId;
                    result.ProductId = ProductId;
                    result.UserId = UserId;
                    result.ProductTypeId = ProductTypeId;
                    result.UnitPrice = UnitPrice;
                    result.TotalPrice = TotalPrice;
                    result.Quantity = Quantity;
                    _context.Add(result);
                    await _context.SaveChangesAsync();
                    return View(productDetail);
                }
                else if (ser != null || shoppingCart != null)
                {
                    shoppingCart = (ShoppingCart)await _context.ShoppingCarts.Where(predicate: p => p.UserId == UserId).FirstOrDefaultAsync(p => p.ProductId == ProductId);

                    if (ser.UserId == UserId && ser.ProductId == ProductId)
                    {


                        //var ito = shop.ShoppingCarts.Find(UserId);
                        //null daw si ito.totalprice
                        //ito.TotalPrice = TotalPrice + TotalPrice;
                        //shop.Entry(ito).CurrentValues.SetValues(TotalPrice);
                        shoppingCart.Id = shoppingCart.Id;
                        shoppingCart.ProductDetailsId = ProductId;
                        shoppingCart.ProductId = ProductId;
                        shoppingCart.UserId = UserId;
                        shoppingCart.ProductTypeId = ProductTypeId;
                        shoppingCart.UnitPrice = UnitPrice;
                        shoppingCart.TotalPrice = shoppingCart.TotalPrice + TotalPrice;
                        shoppingCart.Quantity = shoppingCart.Quantity + Quantity;

                        await _context.SaveChangesAsync();
                        return View(productDetail);
                        //_context.Entry(shoppingCart).State = EntityState.Unchanged;
                        //_context.Attach(shoppingCart);
                        //shoppingCart.TotalPrice = TotalPrice + TotalPrice;

                    }
                    else if (shoppingCart != null)
                    {
                        shoppingCart.Id = shoppingCart.Id;
                        shoppingCart.ProductDetailsId = ProductId;
                        shoppingCart.ProductId = ProductId;
                        shoppingCart.UserId = UserId;
                        shoppingCart.ProductTypeId = ProductTypeId;
                        shoppingCart.UnitPrice = UnitPrice;
                        shoppingCart.TotalPrice = shoppingCart.TotalPrice + TotalPrice;
                        shoppingCart.Quantity = shoppingCart.Quantity + Quantity;

                        await _context.SaveChangesAsync();
                        return View(productDetail);
                    }
                    else
                    {
                        result.Id = ser.Id + 1;
                        result.ProductDetailsId = ProductId;
                        result.ProductId = ProductId;
                        result.UserId = UserId;
                        result.ProductTypeId = ProductTypeId;
                        result.UnitPrice = UnitPrice;
                        result.TotalPrice = TotalPrice;
                        result.Quantity = Quantity;
                        _context.Add(result);
                        await _context.SaveChangesAsync();
                        return View(productDetail);
                    }
                }


            }

            return View(productDetail);
        }
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var productDetail = await _context.ProductDetails
                .Include(p => p.Product)
                .Include(p => p.ProductType)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (productDetail == null)
            {
                return NotFound();
            }

            return View(productDetail);
        }
       
        // GET: Products/Create
        public IActionResult Create()
        {
            ViewData["ProductId"] = new SelectList(_context.Products, "Id", "ProductCode");
            ViewData["ProductTypeId"] = new SelectList(_context.ProductTypes, "Id", "ProductType1");
            return View();
        }

        // POST: Products/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,ProductId,ProductTypeId,AdditionalInfo,Image1,Image2,Image3,Image4,Image5,IsActive,UpdatedAt")] ProductDetail productDetail)
        {
            if (ModelState.IsValid)
            {
                _context.Add(productDetail);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProductId"] = new SelectList(_context.Products, "Id", "ProductCode", productDetail.ProductId);
            ViewData["ProductTypeId"] = new SelectList(_context.ProductTypes, "Id", "ProductType1", productDetail.ProductTypeId);
            return View(productDetail);
        }

        // GET: Products/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var productDetail = await _context.ProductDetails.FindAsync(id);
            if (productDetail == null)
            {
                return NotFound();
            }
            ViewData["ProductId"] = new SelectList(_context.Products, "Id", "ProductCode", productDetail.ProductId);
            ViewData["ProductTypeId"] = new SelectList(_context.ProductTypes, "Id", "ProductType1", productDetail.ProductTypeId);
            return View(productDetail);
        }

        // POST: Products/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,ProductId,ProductTypeId,AdditionalInfo,Image1,Image2,Image3,Image4,Image5,IsActive,UpdatedAt")] ProductDetail productDetail)
        {
            if (id != productDetail.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(productDetail);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProductDetailExists(productDetail.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProductId"] = new SelectList(_context.Products, "Id", "ProductCode", productDetail.ProductId);
            ViewData["ProductTypeId"] = new SelectList(_context.ProductTypes, "Id", "ProductType1", productDetail.ProductTypeId);
            return View(productDetail);
        }

        // GET: Products/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var productDetail = await _context.ProductDetails
                .Include(p => p.Product)
                .Include(p => p.ProductType)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (productDetail == null)
            {
                return NotFound();
            }

            return View(productDetail);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var productDetail = await _context.ProductDetails.FindAsync(id);
            _context.ProductDetails.Remove(productDetail);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ProductDetailExists(int id)
        {
            return _context.ProductDetails.Any(e => e.Id == id);
        }

        public IActionResult remo()
        {
            return View();
        }
    }
}
