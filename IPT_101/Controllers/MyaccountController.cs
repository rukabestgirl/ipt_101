﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IPT_101.Repository;
using IPT_101.Repository.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace IPT_101.Controllers
{
    public class MyaccountController : Controller
    {
        private readonly IPT_101Context _context;

        public MyaccountController(IPT_101Context context)
        {
            _context = context;
        }
        // GET: MyaccountController
        public ActionResult Index()
        {
            return View();
        }

        // GET: MyaccountController/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _context.Users.FindAsync(id);
            if (user == null)
            {
                return NotFound();
            }
            ViewBag.User = user;
            return View(user);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.


        [HttpPost]
        public async Task<IActionResult> woerl(int id, int Id, string Role, string Firstname, string Lastname, int Contactno, string Address, string City, string Province, int Zipcode, string Country, string Email, string Password, User user)
        {

            user = await _context.Users.Where(p => p.Id == Id).FirstOrDefaultAsync();
            user.Id = Id;
            user.Role = Role;
            user.Firstname = Firstname;
            user.Lastname = Lastname;
            user.Contactno = Contactno;
            user.Address = Address;
            user.City = City;
            user.Province = Province;
            user.Zipcode = Zipcode;
            user.Country = Country;
            user.Email = Email;
            user.Password = Password;
            user.UpdatedAt = DateTime.Now;
            await _context.SaveChangesAsync();
            ViewBag.User = user;
            return RedirectToAction(nameof(Index));
        }

        // GET: MyaccountController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: MyaccountController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: MyaccountController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: MyaccountController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: MyaccountController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: MyaccountController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
        private bool UserExists(int id)
        {
            return _context.Users.Any(e => e.Id == id);
        }
    }
}
