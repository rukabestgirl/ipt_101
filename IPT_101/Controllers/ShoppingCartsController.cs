﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using IPT_101.Repository;
using IPT_101.Repository.Models;

namespace IPT_101.Controllers
{
    public class ShoppingCartsController : Controller
    {
        private readonly IPT_101Context _context;

        public ShoppingCartsController(IPT_101Context context)
        {
            _context = context;
        }

        // GET: ShoppingCarts
        public async Task<IActionResult> Index()
        {
            var iPT_101Context = _context.ShoppingCarts.Include(s => s.Product).Include(s => s.ProductDetails).Include(s => s.ProductType).Include(s => s.User);
            return View(await iPT_101Context.ToListAsync());
        }
        [HttpPost]
        public async Task<PartialViewResult> ShoppingCartAsync(int Id)
        {
            List<ShoppingCart> sc = new List<ShoppingCart>();
            sc = await (from ShoppingCart in _context.ShoppingCarts
                        .Include(s => s.Product)
                        .Include(s => s.ProductDetails)
                        .Include(s => s.ProductType)
                        .Include(s => s.User)
                        .Where(p => p.UserId == Id)
                        select ShoppingCart).ToListAsync();
            var result = _context.ShoppingCarts
                             .Include(s => s.Product)
                             .Include(s => s.ProductDetails)
                             .Include(s => s.ProductType)
                             .Include(s => s.User)
                             .Where(p => p.UserId == Id);
            
            var ser = await _context.ShoppingCarts.Where(p => p.UserId == Id).ToListAsync();

            
            //ViewBag.CartCount = ser.Count();
            return PartialView("_Shopcart", sc);
        }
        [HttpPost]
        public async Task<IActionResult> Add(int Id, int UnitPrice)
        {

            var shoppingCart = await _context.ShoppingCarts
                .Include(s => s.Product)
                .Include(s => s.ProductDetails)
                .Include(s => s.ProductType)
                .Include(s => s.User)
                .FirstOrDefaultAsync(m => m.Id == Id);
            shoppingCart.Id = shoppingCart.Id;
            shoppingCart.ProductDetailsId = shoppingCart.ProductId;
            shoppingCart.ProductId = shoppingCart.ProductId;
            shoppingCart.UserId = shoppingCart.UserId;
            shoppingCart.ProductTypeId = shoppingCart.ProductTypeId;
            shoppingCart.UnitPrice = UnitPrice;
            shoppingCart.Quantity = shoppingCart.Quantity + 1;
            shoppingCart.TotalPrice = shoppingCart.TotalPrice + UnitPrice;
            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }
        [HttpPost]
        public async Task<IActionResult> Minus(int Id, int UnitPrice)
        {

            var shoppingCart = await _context.ShoppingCarts
                .Include(s => s.Product)
                .Include(s => s.ProductDetails)
                .Include(s => s.ProductType)
                .Include(s => s.User)
                .FirstOrDefaultAsync(m => m.Id == Id);
            
            if(shoppingCart.Quantity > 1)
            {
                shoppingCart.Id = shoppingCart.Id;
                shoppingCart.ProductDetailsId = shoppingCart.ProductId;
                shoppingCart.ProductId = shoppingCart.ProductId;
                shoppingCart.UserId = shoppingCart.UserId;
                shoppingCart.ProductTypeId = shoppingCart.ProductTypeId;
                shoppingCart.UnitPrice = UnitPrice;
                shoppingCart.Quantity = shoppingCart.Quantity - 1;
                shoppingCart.TotalPrice = shoppingCart.TotalPrice - UnitPrice;
            }
            else
            {
                _context.ShoppingCarts.Remove(shoppingCart);
            }

            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }
        [HttpPost]
        public async Task<IActionResult> Alis(int Id)
        {

            var shoppingCart = await _context.ShoppingCarts
                .Include(s => s.Product)
                .Include(s => s.ProductDetails)
                .Include(s => s.ProductType)
                .Include(s => s.User)
                .FirstOrDefaultAsync(m => m.Id == Id);

            _context.ShoppingCarts.Remove(shoppingCart);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
        // GET: ShoppingCarts/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var shoppingCart = await _context.ShoppingCarts
                .Include(s => s.Product)
                .Include(s => s.ProductDetails)
                .Include(s => s.ProductType)
                .Include(s => s.User)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (shoppingCart == null)
            {
                return NotFound();
            }

            return View(shoppingCart);
        }

        // GET: ShoppingCarts/Create
        public IActionResult Create()
        {
            ViewData["ProductId"] = new SelectList(_context.Products, "Id", "ProductCode");
            ViewData["ProductDetailsId"] = new SelectList(_context.ProductDetails, "Id", "AdditionalInfo");
            ViewData["ProductTypeId"] = new SelectList(_context.ProductTypes, "Id", "ProductType1");
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Address");
            return View();
        }

        // POST: ShoppingCarts/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,ProductId,ProductTypeId,ProductDetailsId,UserId,UnitPrice,TotalPrice,Quantity")] ShoppingCart shoppingCart)
        {
            if (ModelState.IsValid)
            {
                _context.Add(shoppingCart);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProductId"] = new SelectList(_context.Products, "Id", "ProductCode", shoppingCart.ProductId);
            ViewData["ProductDetailsId"] = new SelectList(_context.ProductDetails, "Id", "AdditionalInfo", shoppingCart.ProductDetailsId);
            ViewData["ProductTypeId"] = new SelectList(_context.ProductTypes, "Id", "ProductType1", shoppingCart.ProductTypeId);
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Address", shoppingCart.UserId);
            return View(shoppingCart);
        }

        // GET: ShoppingCarts/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var shoppingCart = await _context.ShoppingCarts.FindAsync(id);
            if (shoppingCart == null)
            {
                return NotFound();
            }
            ViewData["ProductId"] = new SelectList(_context.Products, "Id", "ProductCode", shoppingCart.ProductId);
            ViewData["ProductDetailsId"] = new SelectList(_context.ProductDetails, "Id", "AdditionalInfo", shoppingCart.ProductDetailsId);
            ViewData["ProductTypeId"] = new SelectList(_context.ProductTypes, "Id", "ProductType1", shoppingCart.ProductTypeId);
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Address", shoppingCart.UserId);
            return View(shoppingCart);
        }

        // POST: ShoppingCarts/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,ProductId,ProductTypeId,ProductDetailsId,UserId,UnitPrice,TotalPrice,Quantity")] ShoppingCart shoppingCart)
        {
            if (id != shoppingCart.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(shoppingCart);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ShoppingCartExists(shoppingCart.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProductId"] = new SelectList(_context.Products, "Id", "ProductCode", shoppingCart.ProductId);
            ViewData["ProductDetailsId"] = new SelectList(_context.ProductDetails, "Id", "AdditionalInfo", shoppingCart.ProductDetailsId);
            ViewData["ProductTypeId"] = new SelectList(_context.ProductTypes, "Id", "ProductType1", shoppingCart.ProductTypeId);
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Address", shoppingCart.UserId);
            return View(shoppingCart);
        }

        // GET: ShoppingCarts/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var shoppingCart = await _context.ShoppingCarts
                .Include(s => s.Product)
                .Include(s => s.ProductDetails)
                .Include(s => s.ProductType)
                .Include(s => s.User)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (shoppingCart == null)
            {
                return NotFound();
            }

            return View(shoppingCart);
        }

        // POST: ShoppingCarts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var shoppingCart = await _context.ShoppingCarts.FindAsync(id);
            _context.ShoppingCarts.Remove(shoppingCart);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ShoppingCartExists(int id)
        {
            return _context.ShoppingCarts.Any(e => e.Id == id);
        }
    }
}
