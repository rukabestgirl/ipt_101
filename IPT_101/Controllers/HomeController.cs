﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using IPT_101.Models;
using IPT_101.Repository;
using Microsoft.EntityFrameworkCore;
using IPT_101.Repository.Models;

namespace IPT_101.Controllers
{
    public class HomeController : Controller
    {
        //private readonly ILogger<HomeController> _logger;
        private readonly IPT_101Context _context;

        public HomeController(IPT_101Context context)
        {
            _context = context;
        }
       // public HomeController(ILogger<HomeController> logger)
        //{
         //   _logger = logger;
       // }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public async Task<PartialViewResult> CartCountAsync(int Id)
        {
            List<ShoppingCart> sc = new List<ShoppingCart>();
            sc = await (from ShoppingCart in _context.ShoppingCarts.Where(p => p.UserId == Id)
                        select ShoppingCart).ToListAsync();
            var result =  _context.ShoppingCarts
                             .Include(s => s.Product)
                             .Include(s => s.ProductDetails)
                             .Include(s => s.ProductType)
                             .Include(s => s.User)
                             .Where(p => p.UserId == Id);

            var ser = await _context.ShoppingCarts.Where(p => p.UserId == Id).ToListAsync();
            //ViewBag.CartCount = ser.Count();
            return PartialView("_Wowers", sc);
        }

    }
}
