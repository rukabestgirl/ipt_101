﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using IPT_101.Repository;
using IPT_101.Repository.Models;

namespace IPT_101.Controllers
{
    public class AdminController : Controller
    {
        private readonly IPT_101Context _context;

        public AdminController(IPT_101Context context)
        {
            _context = context;
        }

        // GET: Admin
        public async Task<IActionResult> Index()
        {
            var iPT_101Context = _context.Orders.Include(o => o.Product).Include(o => o.ProductDetails).Include(o => o.ProductType).Include(o => o.User);
            return View(await iPT_101Context.ToListAsync());
        }
        public async Task<IActionResult> Users()
        {
            var iPT_101Context = _context.Users;
            return View(await iPT_101Context.ToListAsync());
        }
        public async Task<IActionResult> Products()
        {
            var iPT_101Context = _context.ProductDetails.Include(p => p.Product).Include(p => p.ProductType);
            return View(await iPT_101Context.ToListAsync());
        }
        public async Task<IActionResult> Orders()
        {
            var iPT_101Context = _context.Orders.Include(o => o.Product).Include(o => o.ProductDetails).Include(o => o.ProductType).Include(o => o.User);
            return View(await iPT_101Context.ToListAsync());
        }
        // GET: Admin/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _context.Orders
                .Include(o => o.Product)
                .Include(o => o.ProductDetails)
                .Include(o => o.ProductType)
                .Include(o => o.User)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (order == null)
            {
                return NotFound();
            }

            return View(order);
        }

        // GET: Admin/Create
        public IActionResult Create()
        {
            ViewData["ProductId"] = new SelectList(_context.Products, "Id", "ProductCode");
            ViewData["ProductDetailsId"] = new SelectList(_context.ProductDetails, "Id", "AdditionalInfo");
            ViewData["ProductTypeId"] = new SelectList(_context.ProductTypes, "Id", "ProductType1");
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Address");
            return View();
        }

        // POST: Admin/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,ProductId,ProductTypeId,ProductDetailsId,UserId,Quantity,PreparationInDays,ShipmentInDays,Courier,TrackingNo,Total,Status")] Order order)
        {
            if (ModelState.IsValid)
            {
                _context.Add(order);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProductId"] = new SelectList(_context.Products, "Id", "ProductCode", order.ProductId);
            ViewData["ProductDetailsId"] = new SelectList(_context.ProductDetails, "Id", "AdditionalInfo", order.ProductDetailsId);
            ViewData["ProductTypeId"] = new SelectList(_context.ProductTypes, "Id", "ProductType1", order.ProductTypeId);
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Address", order.UserId);
            return View(order);
        }

        // GET: Admin/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _context.Orders.FindAsync(id);
            if (order == null)
            {
                return NotFound();
            }
            ViewData["ProductId"] = new SelectList(_context.Products, "Id", "ProductCode", order.ProductId);
            ViewData["ProductDetailsId"] = new SelectList(_context.ProductDetails, "Id", "AdditionalInfo", order.ProductDetailsId);
            ViewData["ProductTypeId"] = new SelectList(_context.ProductTypes, "Id", "ProductType1", order.ProductTypeId);
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Address", order.UserId);
            return View(order);
        }

        // POST: Admin/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,ProductId,ProductTypeId,ProductDetailsId,UserId,Quantity,PreparationInDays,ShipmentInDays,Courier,TrackingNo,Total,Status")] Order order)
        {
            if (id != order.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(order);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!OrderExists(order.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProductId"] = new SelectList(_context.Products, "Id", "ProductCode", order.ProductId);
            ViewData["ProductDetailsId"] = new SelectList(_context.ProductDetails, "Id", "AdditionalInfo", order.ProductDetailsId);
            ViewData["ProductTypeId"] = new SelectList(_context.ProductTypes, "Id", "ProductType1", order.ProductTypeId);
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Address", order.UserId);
            return View(order);
        }

        // GET: Admin/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _context.Orders
                .Include(o => o.Product)
                .Include(o => o.ProductDetails)
                .Include(o => o.ProductType)
                .Include(o => o.User)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (order == null)
            {
                return NotFound();
            }

            return View(order);
        }

        // POST: Admin/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var order = await _context.Orders.FindAsync(id);
            _context.Orders.Remove(order);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool OrderExists(int id)
        {
            return _context.Orders.Any(e => e.Id == id);
        }
    }
}
