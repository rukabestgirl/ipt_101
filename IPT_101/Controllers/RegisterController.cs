﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IPT_101.Models;
using IPT_101.Repository;
using IPT_101.Repository.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace IPT_101.Controllers
{
    public class RegisterController : Controller
    {
        private readonly IPT_101Context _context;
        // GET: RegisterController
        public RegisterController(IPT_101Context context)
        {
            _context = context;
        }

        db dbop = new db();
        public IActionResult Index(int id=0)
        {
            User us = new User();
            var ser = _context.Users.OrderByDescending(c => c.Id).FirstOrDefault();


            if (id != 0)
            {
                us = _context.Users.Where(x => x.Id == id).FirstOrDefault<User>();
            }
            else if (ser == null)
            {
                us.Id = 1009;
            }
            else
            {
                us.Id = ser.Id + 1;
            }
            return View(us);
        }
        [HttpPost]
        public async Task<IActionResult> IndexAsync(string password, string repassword,[Bind("Id,Role,Firstname,Lastname,Contactno,Address,City,Province,Zipcode,Country,Email,Password,Isactive,UpdatedAt")] User user)
        {
           
            if(password != repassword)
            {
                TempData["msg"] = "Password doesn't match!";
            }
            else
            {
                int res = dbop.EmailCheck(user);
                if (res == 1)
                {
                    TempData["msg"] = "Meron na po";
                }
                else
                {
                    TempData["msg"] = "Registered Successfully";
                    if (ModelState.IsValid)
                    {

                        _context.Add(user);
                        await _context.SaveChangesAsync();
                    }
                }
            }
            return View();
        }
        // GET: RegisterController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: RegisterController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: RegisterController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: RegisterController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: RegisterController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: RegisterController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: RegisterController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
