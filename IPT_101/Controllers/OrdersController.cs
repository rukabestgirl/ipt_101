﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using IPT_101.Repository;
using IPT_101.Repository.Models;

namespace IPT_101.Controllers
{
    public class OrdersController : Controller
    {
        private readonly IPT_101Context _context;

        public OrdersController(IPT_101Context context)
        {
            _context = context;
        }

        // GET: Orders
        public async Task<IActionResult> Index(int UserId)
        {
            var iPT_101Context = _context.Orders.Include(o => o.Product).Include(o => o.ProductDetails).Include(o => o.ProductType).Include(o => o.User).Where(s => s.UserId == UserId);
            return View(await iPT_101Context.ToListAsync());
        }
        [HttpPost]
        public async Task<PartialViewResult> OrderAsync(int Id)
        {
            List<Order> sc = new List<Order>();
            sc = await (from Order in _context.Orders
                        .Include(s => s.Product)
                        .Include(s => s.ProductDetails)
                        .Include(s => s.ProductType)
                        .Include(s => s.User)
                        .Where(p => p.UserId == Id)
                        select Order).ToListAsync();
            var result = await _context.ShoppingCarts
                             .Include(s => s.Product)
                             .Include(s => s.ProductDetails)
                             .Include(s => s.ProductType)
                             .Include(s => s.User)
                             .Where(p => p.UserId == Id)
                             .FirstOrDefaultAsync();

            var ser = await _context.ShoppingCarts.Where(p => p.UserId == Id).ToListAsync();
            //ViewBag.CartCount = ser.Count();
            //ViewBag.FirstName = result.User.Firstname;
            //ViewBag.Lastname = result.User.Lastname;
            //ViewBag.Address = result.User.Address;
            //ViewBag.City = result.User.City;
            //ViewBag.Province = result.User.Province;
            //ViewBag.Country = result.User.Country;
            //ViewBag.Lastname = result.User.Lastname;
            return PartialView("_Orders", sc);
        }
        [HttpPost]
        //need tanggalin to 
        public async Task<IActionResult> Place_Order(int UserId, Order orders, int id = 0)
        {
            List<ShoppingCart> sc = new List<ShoppingCart>();
            sc = await (from ShoppingCart in _context.ShoppingCarts
                        .Include(s => s.Product)
                        .Include(s => s.ProductDetails)
                        .Include(s => s.ProductType)
                        .Include(s => s.User)
                        .Where(p => p.UserId == UserId)
                        select ShoppingCart).ToListAsync();



            var ser = await _context.Orders.OrderByDescending(c => c.Id).FirstOrDefaultAsync();
            var scorders =  _context.ShoppingCarts.FindAsync(UserId);

            var order = new Order();

            var results =  _context.ShoppingCarts
                             .Include(s => s.Product)
                             .Include(s => s.ProductDetails)
                             .Include(s => s.ProductType)
                             .Include(s => s.User)
                             .Where(p => p.UserId == UserId);
            
            //List<Order> ord = new List<Order>();
            if (ser == null)
            {
                if (sc.Count() > 1)
                {
                    string wewers = id.ToString();
                    foreach (var item in sc)
                    {
                        order.Id = id;
                        order.ProductId = item.ProductId;
                        order.ProductTypeId = item.ProductTypeId;
                        order.ProductDetailsId = item.ProductDetailsId;
                        order.UserId = item.UserId;
                        order.Quantity = item.Quantity;
                        order.PreparationInDays = item.Product.ParcelPreparationInDays;
                        order.ShipmentInDays = item.Product.ParcelPreparationInDays;
                        order.Courier = "LBC";
                        order.TrackingNo = "#1022363" +wewers;
                        order.Total = item.TotalPrice;
                        order.Status = "ORDERED";
                        _context.Add(order);
                        id++;
                        _context.SaveChanges();
                    }
                }
                else
                {
                    foreach (var item in sc)
                    {
                        order.Id = 0;
                        order.ProductId = item.ProductId;
                        order.ProductTypeId = item.ProductTypeId;
                        order.ProductDetailsId = item.ProductDetailsId;
                        order.UserId = item.UserId;
                        order.Quantity = item.Quantity;
                        order.PreparationInDays = item.Product.ParcelPreparationInDays;
                        order.ShipmentInDays = item.Product.ParcelPreparationInDays;
                        order.Courier = "LBC";
                        order.TrackingNo = "#1022363" + order.Id.ToString();
                        order.Total = item.TotalPrice;
                        order.Status = "ORDERED";
                        _context.Add(order);
                        _context.SaveChanges();
                    }
                }
                
            }
            else if (ser != null)
            {
                if (sc.Count() > 1)
                {
                    
                    int itopo = 1;
                    string wewers = (ser.Id + itopo).ToString();
                    foreach (var item in sc)
                    {
                        
                        order.Id = ser.Id + itopo;
                        order.ProductId = item.ProductId;
                        order.ProductTypeId = item.ProductTypeId;
                        order.ProductDetailsId = item.ProductDetailsId;
                        order.UserId = item.UserId;
                        order.Quantity = item.Quantity;
                        order.PreparationInDays = item.Product.ParcelPreparationInDays;
                        order.ShipmentInDays = item.Product.ParcelPreparationInDays;
                        order.Courier = "LBC";
                        order.TrackingNo = "#1022363" + wewers;
                        order.Total = item.TotalPrice;
                        order.Status = "ORDERED";
                        _context.Add(order);
                        itopo++;
                        _context.SaveChanges();
                        

                    }
                }
                else
                {
                    foreach (var item in sc)
                    {
                        order.Id = ser.Id + 1;
                        order.ProductId = item.ProductId;
                        order.ProductTypeId = item.ProductTypeId;
                        order.ProductDetailsId = item.ProductDetailsId;
                        order.UserId = item.UserId;
                        order.Quantity = item.Quantity;
                        order.PreparationInDays = item.Product.ParcelPreparationInDays;
                        order.ShipmentInDays = item.Product.ParcelPreparationInDays;
                        order.Courier = "LBC";
                        order.TrackingNo = "#1022363" + order.Id.ToString();
                        order.Total = item.TotalPrice;
                        order.Status = "ORDERED";
                        _context.Add(order);
                        _context.SaveChanges();
                    }
                }
            }

            foreach(var item in sc)
            {
                _context.Remove(item);

            }
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        // GET: Orders/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _context.Orders
                .Include(o => o.Product)
                .Include(o => o.ProductDetails)
                .Include(o => o.ProductType)
                .Include(o => o.User)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (order == null)
            {
                return NotFound();
            }

            return View(order);
        }

        // GET: Orders/Create
        public IActionResult Create()
        {
            ViewData["ProductId"] = new SelectList(_context.Products, "Id", "ProductCode");
            ViewData["ProductDetailsId"] = new SelectList(_context.ProductDetails, "Id", "AdditionalInfo");
            ViewData["ProductTypeId"] = new SelectList(_context.ProductTypes, "Id", "ProductType1");
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Address");
            return View();
        }

        // POST: Orders/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,ProductId,ProductTypeId,ProductDetailsId,UserId,Quantity,PreparationInDays,ShipmentInDays,Courier,TrackingNo,Total,Status")] Order order)
        {
            if (ModelState.IsValid)
            {
                _context.Add(order);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProductId"] = new SelectList(_context.Products, "Id", "ProductCode", order.ProductId);
            ViewData["ProductDetailsId"] = new SelectList(_context.ProductDetails, "Id", "AdditionalInfo", order.ProductDetailsId);
            ViewData["ProductTypeId"] = new SelectList(_context.ProductTypes, "Id", "ProductType1", order.ProductTypeId);
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Address", order.UserId);
            return View(order);
        }

        // GET: Orders/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _context.Orders.FindAsync(id);
            if (order == null)
            {
                return NotFound();
            }
            ViewData["ProductId"] = new SelectList(_context.Products, "Id", "ProductCode", order.ProductId);
            ViewData["ProductDetailsId"] = new SelectList(_context.ProductDetails, "Id", "AdditionalInfo", order.ProductDetailsId);
            ViewData["ProductTypeId"] = new SelectList(_context.ProductTypes, "Id", "ProductType1", order.ProductTypeId);
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Address", order.UserId);
            return View(order);
        }

        // POST: Orders/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,ProductId,ProductTypeId,ProductDetailsId,UserId,Quantity,PreparationInDays,ShipmentInDays,Courier,TrackingNo,Total,Status")] Order order)
        {
            if (id != order.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(order);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!OrderExists(order.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProductId"] = new SelectList(_context.Products, "Id", "ProductCode", order.ProductId);
            ViewData["ProductDetailsId"] = new SelectList(_context.ProductDetails, "Id", "AdditionalInfo", order.ProductDetailsId);
            ViewData["ProductTypeId"] = new SelectList(_context.ProductTypes, "Id", "ProductType1", order.ProductTypeId);
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Address", order.UserId);
            return View(order);
        }

        // GET: Orders/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _context.Orders
                .Include(o => o.Product)
                .Include(o => o.ProductDetails)
                .Include(o => o.ProductType)
                .Include(o => o.User)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (order == null)
            {
                return NotFound();
            }

            return View(order);
        }

        // POST: Orders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var order = await _context.Orders.FindAsync(id);
            _context.Orders.Remove(order);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool OrderExists(int id)
        {
            return _context.Orders.Any(e => e.Id == id);
        }
    }
}
