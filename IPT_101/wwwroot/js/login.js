﻿$("#button-addon1").on("click", () => {
    var getAttrValue = $("#login_account_password").attr('type');
    var newAttrValue = (getAttrValue === "password") ? "text" : "password";
    $("#login_account_password").attr('type', newAttrValue);

    if (getAttrValue === "text") {
        $("#eyecon").removeClass();
        $("#eyecon").addClass("fa fa-eye");
    } else {
        $("#eyecon").removeClass();
        $("#eyecon").addClass("fa fa-eye-slash");
    }
});




