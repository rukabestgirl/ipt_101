﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using IPT_101.Models;
using IPT_101.Repository.Models;

namespace IPT_101.Models
{
    public class db
    {
        SqlConnection con = new SqlConnection("Data Source=HOGAN;Initial Catalog=IPT_101;Integrated Security=True");
        public int LogInCheck(Login login)
        {
            SqlCommand com = new SqlCommand("Login", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@EMAIL",login.Email);
            
            com.Parameters.AddWithValue("@PASSWORD", login.Password);
            //com.Parameters.AddWithValue("@ROLE", login.Role);
            SqlParameter oblogin = new SqlParameter();
            oblogin.ParameterName = "@Isvalid";
            oblogin.SqlDbType = SqlDbType.Bit;
            oblogin.Direction = ParameterDirection.Output;
            com.Parameters.Add(oblogin);
            con.Open();
            com.ExecuteNonQuery();
            int res = Convert.ToInt32(oblogin.Value);
            con.Close();
            return res;
        }
        public int EmailCheck(User user)
        {
            SqlCommand com = new SqlCommand("EmailCheck", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@EMAIL", user.Email);
            SqlParameter oblogin = new SqlParameter();
            oblogin.ParameterName = "@Isvalid";
            oblogin.SqlDbType = SqlDbType.Bit;
            oblogin.Direction = ParameterDirection.Output;
            com.Parameters.Add(oblogin);
            con.Open();
            com.ExecuteNonQuery();
            int res = Convert.ToInt32(oblogin.Value);
            con.Close();
            return res;
        }
    }

    public class dbs
    {
        SqlConnection con = new SqlConnection("Data Source=HOGAN;Initial Catalog=IPT_101;Integrated Security=True");
        public void saheg()
        {
            SqlCommand com = new SqlCommand("Select * FROM products", con);
            con.Open();
            com.ExecuteNonQuery();
            con.Close();
        }
    }
}
