﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IPT_101.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "product_type",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false),
                    PRODUCT_TYPE = table.Column<string>(type: "varchar(max)", unicode: false, nullable: false),
                    IS_ACTIVE = table.Column<short>(type: "smallint", nullable: false),
                    UPDATED_AT = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_product_type", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "products",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false),
                    PRODUCT_CODE = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: false),
                    PRODUCT_NAME = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: false),
                    PRODUCT_DESCRIPTION = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: false),
                    SUGGESTED_UNIT_PRICE = table.Column<int>(type: "int", nullable: false),
                    UNITS_IN_STOCK = table.Column<int>(type: "int", nullable: false),
                    PARCEL_PREPARATION_IN_DAYS = table.Column<int>(type: "int", nullable: false),
                    IS_ACTIVE = table.Column<short>(type: "smallint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_products", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Register",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Role = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Firstname = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Lastname = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Contactno = table.Column<int>(type: "int", nullable: false),
                    Address = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    City = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Province = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Zipcode = table.Column<int>(type: "int", nullable: false),
                    Country = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Password = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Isactive = table.Column<short>(type: "smallint", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Register", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "users",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false),
                    ROLE = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: false),
                    FIRSTNAME = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: false),
                    LASTNAME = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: false),
                    CONTACTNO = table.Column<int>(type: "int", nullable: false),
                    ADDRESS = table.Column<string>(type: "varchar(max)", unicode: false, nullable: false),
                    CITY = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: false),
                    PROVINCE = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: false),
                    ZIPCODE = table.Column<int>(type: "int", nullable: false),
                    COUNTRY = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: false),
                    EMAIL = table.Column<string>(type: "varchar(max)", unicode: false, nullable: false),
                    PASSWORD = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: false),
                    ISACTIVE = table.Column<short>(type: "smallint", nullable: false),
                    UPDATED_AT = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_users", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "product_details",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false),
                    PRODUCT_ID = table.Column<int>(type: "int", nullable: false),
                    PRODUCT_TYPE_ID = table.Column<int>(type: "int", nullable: false),
                    ADDITIONAL_INFO = table.Column<string>(type: "varchar(max)", unicode: false, nullable: false),
                    IMAGE1 = table.Column<string>(type: "varchar(max)", unicode: false, nullable: false),
                    IMAGE2 = table.Column<string>(type: "varchar(max)", unicode: false, nullable: false),
                    IMAGE3 = table.Column<string>(type: "varchar(max)", unicode: false, nullable: false),
                    IMAGE4 = table.Column<string>(type: "varchar(max)", unicode: false, nullable: false),
                    IMAGE5 = table.Column<string>(type: "varchar(max)", unicode: false, nullable: false),
                    IS_ACTIVE = table.Column<short>(type: "smallint", nullable: false),
                    UPDATED_AT = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_product_details", x => x.ID);
                    table.ForeignKey(
                        name: "FK_product_details_product_type",
                        column: x => x.PRODUCT_TYPE_ID,
                        principalTable: "product_type",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_product_details_products",
                        column: x => x.PRODUCT_ID,
                        principalTable: "products",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "orders",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false),
                    PRODUCT_ID = table.Column<int>(type: "int", nullable: true),
                    PRODUCT_TYPE_ID = table.Column<int>(type: "int", nullable: true),
                    PRODUCT_DETAILS_ID = table.Column<int>(type: "int", nullable: true),
                    USER_ID = table.Column<int>(type: "int", nullable: true),
                    QUANTITY = table.Column<int>(type: "int", nullable: true),
                    PREPARATION_IN_DAYS = table.Column<int>(type: "int", nullable: true),
                    SHIPMENT_IN_DAYS = table.Column<int>(type: "int", nullable: true),
                    COURIER = table.Column<string>(type: "varchar(max)", unicode: false, nullable: true),
                    TRACKING_NO = table.Column<string>(type: "varchar(max)", unicode: false, nullable: true),
                    TOTAL = table.Column<int>(type: "int", nullable: true),
                    STATUS = table.Column<string>(type: "varchar(max)", unicode: false, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_orders", x => x.ID);
                    table.ForeignKey(
                        name: "FK_orders_product_details",
                        column: x => x.PRODUCT_DETAILS_ID,
                        principalTable: "product_details",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_orders_product_type",
                        column: x => x.PRODUCT_TYPE_ID,
                        principalTable: "product_type",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_orders_products",
                        column: x => x.PRODUCT_ID,
                        principalTable: "products",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_orders_users",
                        column: x => x.USER_ID,
                        principalTable: "users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "shopping_cart",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false),
                    PRODUCT_ID = table.Column<int>(type: "int", nullable: false),
                    PRODUCT_TYPE_ID = table.Column<int>(type: "int", nullable: false),
                    PRODUCT_DETAILS_ID = table.Column<int>(type: "int", nullable: false),
                    USER_ID = table.Column<int>(type: "int", nullable: false),
                    UNIT_PRICE = table.Column<int>(type: "int", nullable: false),
                    TOTAL_PRICE = table.Column<int>(type: "int", nullable: false),
                    QUANTITY = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_shopping_cart", x => x.ID);
                    table.ForeignKey(
                        name: "FK_shopping_cart_product_details",
                        column: x => x.PRODUCT_DETAILS_ID,
                        principalTable: "product_details",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_shopping_cart_product_type",
                        column: x => x.PRODUCT_TYPE_ID,
                        principalTable: "product_type",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_shopping_cart_products",
                        column: x => x.PRODUCT_ID,
                        principalTable: "products",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_shopping_cart_users",
                        column: x => x.USER_ID,
                        principalTable: "users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_orders_PRODUCT_DETAILS_ID",
                table: "orders",
                column: "PRODUCT_DETAILS_ID");

            migrationBuilder.CreateIndex(
                name: "IX_orders_PRODUCT_ID",
                table: "orders",
                column: "PRODUCT_ID");

            migrationBuilder.CreateIndex(
                name: "IX_orders_PRODUCT_TYPE_ID",
                table: "orders",
                column: "PRODUCT_TYPE_ID");

            migrationBuilder.CreateIndex(
                name: "IX_orders_USER_ID",
                table: "orders",
                column: "USER_ID");

            migrationBuilder.CreateIndex(
                name: "IX_product_details_PRODUCT_ID",
                table: "product_details",
                column: "PRODUCT_ID");

            migrationBuilder.CreateIndex(
                name: "IX_product_details_PRODUCT_TYPE_ID",
                table: "product_details",
                column: "PRODUCT_TYPE_ID");

            migrationBuilder.CreateIndex(
                name: "IX_shopping_cart_PRODUCT_DETAILS_ID",
                table: "shopping_cart",
                column: "PRODUCT_DETAILS_ID");

            migrationBuilder.CreateIndex(
                name: "IX_shopping_cart_PRODUCT_ID",
                table: "shopping_cart",
                column: "PRODUCT_ID");

            migrationBuilder.CreateIndex(
                name: "IX_shopping_cart_PRODUCT_TYPE_ID",
                table: "shopping_cart",
                column: "PRODUCT_TYPE_ID");

            migrationBuilder.CreateIndex(
                name: "IX_shopping_cart_USER_ID",
                table: "shopping_cart",
                column: "USER_ID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "orders");

            migrationBuilder.DropTable(
                name: "Register");

            migrationBuilder.DropTable(
                name: "shopping_cart");

            migrationBuilder.DropTable(
                name: "product_details");

            migrationBuilder.DropTable(
                name: "users");

            migrationBuilder.DropTable(
                name: "product_type");

            migrationBuilder.DropTable(
                name: "products");
        }
    }
}
