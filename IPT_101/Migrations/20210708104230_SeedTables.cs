﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IPT_101.Migrations
{
    public partial class SeedTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "product_type",
                columns: new[] { "ID", "IS_ACTIVE", "PRODUCT_TYPE", "UPDATED_AT" },
                values: new object[,]
                {
                    { 2, (short)1, "Accessories", new DateTime(2021, 7, 8, 18, 42, 29, 488, DateTimeKind.Local).AddTicks(4126) },
                    { 1, (short)1, "Smartphones", new DateTime(2021, 7, 8, 18, 42, 29, 488, DateTimeKind.Local).AddTicks(3641) }
                });

            migrationBuilder.InsertData(
                table: "products",
                columns: new[] { "ID", "IS_ACTIVE", "PARCEL_PREPARATION_IN_DAYS", "PRODUCT_CODE", "PRODUCT_DESCRIPTION", "PRODUCT_NAME", "SUGGESTED_UNIT_PRICE", "UNITS_IN_STOCK" },
                values: new object[,]
                {
                    { 29, (short)1, 5, "Prod-29-6292021", "This phone will make you boom-boom.", "Apple iPhone 8", 5128, 728 },
                    { 30, (short)1, 5, "Prod-30-6292021", "This phone will make you boom-boom.", "Apple iPhone XR", 3652, 887 },
                    { 31, (short)1, 5, "Prod-31-6292021", "This phone will make you boom-boom.", "Realme C2 2020", 3816, 390 },
                    { 32, (short)1, 5, "Prod-32-6292021", "This phone will make you boom-boom.", "Realme C21Y", 7809, 211 },
                    { 33, (short)1, 5, "Prod-33-6292021", "This phone will make you boom-boom.", "Realme C15", 8357, 21 },
                    { 34, (short)1, 5, "Prod-34-6292021", "This phone will make you boom-boom.", "Realme C12", 3863, 259 },
                    { 35, (short)1, 5, "Prod-35-6292021", "This phone will make you boom-boom.", "Realme C11", 1925, 192 },
                    { 36, (short)1, 5, "Prod-36-6292021", "This phone will make you boom-boom.", "Realme 8", 2265, 804 },
                    { 37, (short)1, 5, "Prod-37-6292021", "This phone will make you boom-boom.", "Realme 7 PRO", 6101, 807 },
                    { 38, (short)1, 5, "Prod-38-6292021", "This phone will make you boom-boom.", "Realme 7", 2272, 482 },
                    { 39, (short)1, 5, "Prod-39-6292021", "This phone will make you boom-boom.", "Realme 6i", 6353, 784 },
                    { 40, (short)1, 5, "Prod-40-6292021", "This phone will make you boom-boom.", "Realme 6 PRO", 7950, 805 },
                    { 41, (short)1, 5, "Prod-41-6292021", "This phone will make you boom-boom.", "Huawei P30 PRO", 4957, 983 },
                    { 42, (short)1, 5, "Prod-42-6292021", "This phone will make you boom-boom.", "Huawei MATE 30 PRO", 1689, 327 },
                    { 43, (short)1, 5, "Prod-43-6292021", "This phone will make you boom-boom.", "Huawei MATE 30", 3078, 497 },
                    { 44, (short)1, 5, "Prod-44-6292021", "This phone will make you boom-boom.", "Huawei NOVA 5T", 1397, 475 },
                    { 45, (short)1, 5, "Prod-45-6292021", "This phone will make you boom-boom.", "Huawei NOVA 7i", 9439, 832 },
                    { 46, (short)1, 5, "Prod-46-6292021", "This phone will make you boom-boom.", "Huawei NOVA 7SE", 9708, 561 },
                    { 47, (short)1, 5, "Prod-47-6292021", "This phone will make you boom-boom.", "Huawei P40", 5618, 336 },
                    { 48, (short)1, 5, "Prod-48-6292021", "This phone will make you boom-boom.", "Huawei Y9S", 7022, 546 },
                    { 49, (short)1, 5, "Prod-49-6292021", "This phone will make you boom-boom.", "Huawei Y8P", 2134, 53 },
                    { 50, (short)1, 5, "Prod-50-6292021", "This phone will make you boom-boom.", "Huawei Y7P", 1690, 142 },
                    { 28, (short)1, 5, "Prod-28-6292021", "This phone will make you boom-boom.", "Apple iPhone 7+", 9425, 549 },
                    { 27, (short)1, 5, "Prod-27-6292021", "This phone will make you boom-boom.", "Apple iPhone 12", 6111, 888 },
                    { 1, (short)1, 5, "Prod-1-6292021", "This phone will make you boom-boom.", "Oppo A74", 4693, 138 },
                    { 25, (short)1, 5, "Prod-25-6292021", "This phone will make you boom-boom.", "Apple iPhone 11 Pro", 9295, 790 },
                    { 2, (short)1, 5, "Prod-2-6292021", "This phone will make you boom-boom.", "Oppo A92", 6985, 823 },
                    { 3, (short)1, 5, "Prod-3-6292021", "This phone will make you boom-boom.", "Oppo A94", 9045, 238 },
                    { 4, (short)1, 5, "Prod-4-6292021", "This phone will make you boom-boom.", "Oppo Reno 3 Pro", 4507, 875 },
                    { 5, (short)1, 5, "Prod-5-6292021", "This phone will make you boom-boom.", "Oppo Reno 5", 7968, 672 },
                    { 6, (short)1, 5, "Prod-6-6292021", "This phone will make you boom-boom.", "Oppo A5S", 6698, 683 },
                    { 7, (short)1, 5, "Prod-7-6292021", "This phone will make you boom-boom.", "Oppo Reno 4", 2207, 486 },
                    { 26, (short)1, 5, "Prod-26-6292021", "This phone will make you boom-boom.", "Apple iPhone 11 Pro Max", 3987, 357 },
                    { 9, (short)1, 5, "Prod-9-6292021", "This phone will make you boom-boom.", "Oppo A12", 6517, 791 },
                    { 10, (short)1, 5, "Prod-10-6292021", "This phone will make you boom-boom.", "Oppo Reno 2F", 8110, 80 },
                    { 11, (short)1, 5, "Prod-11-6292021", "This phone will make you boom-boom.", "Samsung Galaxy A50S", 4294, 858 },
                    { 12, (short)1, 5, "Prod-12-6292021", "This phone will make you boom-boom.", "Samsung Galaxy A01", 7117, 934 },
                    { 8, (short)1, 5, "Prod-8-6292021", "This phone will make you boom-boom.", "Oppo A15S", 6574, 431 },
                    { 14, (short)1, 5, "Prod-14-6292021", "This phone will make you boom-boom.", "Samsung Galaxy S20+", 9329, 997 },
                    { 13, (short)1, 5, "Prod-13-6292021", "This phone will make you boom-boom.", "Samsung Galaxy A11", 5767, 5 }
                });

            migrationBuilder.InsertData(
                table: "products",
                columns: new[] { "ID", "IS_ACTIVE", "PARCEL_PREPARATION_IN_DAYS", "PRODUCT_CODE", "PRODUCT_DESCRIPTION", "PRODUCT_NAME", "SUGGESTED_UNIT_PRICE", "UNITS_IN_STOCK" },
                values: new object[,]
                {
                    { 23, (short)1, 5, "Prod-23-6292021", "This phone will make you boom-boom.", "Apple iPhone 12 Pro Max", 2471, 722 },
                    { 22, (short)1, 5, "Prod-22-6292021", "This phone will make you boom-boom.", "Apple iPhone SE", 3384, 893 },
                    { 21, (short)1, 5, "Prod-21-6292021", "This phone will make you boom-boom.", "Apple iPhone 6s", 9447, 881 },
                    { 20, (short)1, 5, "Prod-20-6292021", "This phone will make you boom-boom.", "Samsung Galaxy A52", 3026, 981 },
                    { 24, (short)1, 5, "Prod-24-6292021", "This phone will make you boom-boom.", "Apple iPhone 11", 6092, 939 },
                    { 18, (short)1, 5, "Prod-18-6292021", "This phone will make you boom-boom.", "Samsung Galaxy A71", 4454, 425 },
                    { 17, (short)1, 5, "Prod-17-6292021", "This phone will make you boom-boom.", "Samsung Galaxy A30S", 4294, 873 },
                    { 16, (short)1, 5, "Prod-16-6292021", "This phone will make you boom-boom.", "Samsung Galaxy Note 10+", 8704, 611 },
                    { 15, (short)1, 5, "Prod-15-6292021", "This phone will make you boom-boom.", "Samsung Galaxy Note 20", 2075, 562 },
                    { 19, (short)1, 5, "Prod-19-6292021", "This phone will make you boom-boom.", "Samsung Galaxy A72", 3618, 807 }
                });

            migrationBuilder.InsertData(
                table: "users",
                columns: new[] { "ID", "ADDRESS", "CITY", "CONTACTNO", "COUNTRY", "EMAIL", "FIRSTNAME", "ISACTIVE", "LASTNAME", "PASSWORD", "PROVINCE", "ROLE", "UPDATED_AT", "ZIPCODE" },
                values: new object[,]
                {
                    { 1027, "Road 2 st, Brgy. Project 6 , Quezon City", "Quezon City", 912332122, "Philippines", "bogart23@gmail.com", "Bogart", (short)1, "Awaw", "akosibogart2", "Metro Manila", "CUSTOMER", new DateTime(2021, 7, 8, 18, 42, 29, 489, DateTimeKind.Local).AddTicks(1440), 1100 },
                    { 1028, "Congressional Avenue st, Brgy. Ramon Magsaysay , Quezon City", "Quezon City", 964343434, "Philippines", "cathymaninang@gmail.com", "Mia", (short)1, "Kalipaparo", "akosimiaganda12", "Metro Manila", "CUSTOMER", new DateTime(2021, 7, 8, 18, 42, 29, 489, DateTimeKind.Local).AddTicks(1447), 1105 },
                    { 1029, "98 Bulusan st, Brgy. Salvacion , Quezon City", "Quezon City", 934334233, "Philippines", "Cocopogi@gmail.com", "Coco", (short)1, "Ricapo", "cocomartin2", "Metro Manila", "CUSTOMER", new DateTime(2021, 7, 8, 18, 42, 29, 489, DateTimeKind.Local).AddTicks(1450), 1114 },
                    { 1030, "77 Del Pilar st, Brgy. San Antonio , Quezon City", "Quezon City", 918746323, "Philippines", "Tinamorata@gmail.com", "Tina", (short)1, "Morata", "tinamoranmorata4", "Metro Manila", "CUSTOMER", new DateTime(2021, 7, 8, 18, 42, 29, 489, DateTimeKind.Local).AddTicks(1452), 1105 },
                    { 1031, "Halcon st, Brgy. San Isidro Salvador , Quezon City", "Quezon City", 916444353, "Philippines", "bardagul@gmail.com", "Bard", (short)1, "Dagul", "pashpash", "Metro Manila", "CUSTOMER", new DateTime(2021, 7, 8, 18, 42, 29, 489, DateTimeKind.Local).AddTicks(1455), 1113 },
                    { 1032, "9 Lawis st, Brgy. San Jose , Quezon City", "Quezon City", 965544345, "Philippines", "Marktagor5@gmail.com", "Mark", (short)1, "Tagor", "Mahalkosiex2", "Metro Manila", "CUSTOMER", new DateTime(2021, 7, 8, 18, 42, 29, 489, DateTimeKind.Local).AddTicks(1457), 1115 },
                    { 1033, "8 Sto. Domingo st, Brgy. Sienna , Quezon City", "Quezon City", 965434246, "Philippines", "Fatympasandalan@gmail.com", "Fatym", (short)1, "Pasandalan", "bhosmaldita4", "Metro Manila", "CUSTOMER", new DateTime(2021, 7, 8, 18, 42, 29, 489, DateTimeKind.Local).AddTicks(1460), 1114 },
                    { 1034, "875 Santa Catalina, Brgy. St. Peter , Quezon City", "Quezon City", 966545222, "Philippines", "Jammagno@gmail.com", "Jam", (short)1, "Magno", "Barbielat56", "Metro Manila", "CUSTOMER", new DateTime(2021, 7, 8, 18, 42, 29, 489, DateTimeKind.Local).AddTicks(1463), 1114 },
                    { 1035, "P.Florentino st, Brgy. Santa Cruz , Quezon City", "Quezon City", 965355234, "Philippines", "Pedropenduko@gmail.com", "Pedto", (short)1, "Penduko", "agimat22o", "Metro Manila", "CUSTOMER", new DateTime(2021, 7, 8, 18, 42, 29, 489, DateTimeKind.Local).AddTicks(1489), 1104 },
                    { 1036, "11 Mayon st, Brgy. Sta. Teresita , Quezon City", "Quezon City", 963454233, "Philippines", "JessicaSuho@gmail.com", "Jessica", (short)1, "Suho", "memberakongexo", "Metro Manila", "CUSTOMER", new DateTime(2021, 7, 8, 18, 42, 29, 489, DateTimeKind.Local).AddTicks(1492), 1114 },
                    { 1048, "Gremville Subdivision, Brgy. Bagbaguin , Caloocan City", "Caloocan City", 964345446, "Philippines", "arielcalipay@gmail.com", "Ariel", (short)1, "Calipay", "boijackpot3", "Metro Manila", "CUSTOMER", new DateTime(2021, 7, 8, 18, 42, 29, 489, DateTimeKind.Local).AddTicks(1528), 1440 },
                    { 1038, "09 Sarsa st, Brgy. Sto. Domingo , Quezon City", "Quezon City", 966533323, "Philippines", "Kennethregis@gmail.com", "Kenneth", (short)1, "Regis", "aylabklaris", "Metro Manila", "CUSTOMER", new DateTime(2021, 7, 8, 18, 42, 29, 489, DateTimeKind.Local).AddTicks(1498), 1114 },
                    { 1039, "099 Calamba st, Brgy. Talayan , Quezon City", "Quezon City", 987777655, "Philippines", "JadeOloroso@gmail.com", "Jade", (short)1, "Oloroso", "Mahalkojowaq12", "Metro Manila", "CUSTOMER", new DateTime(2021, 7, 8, 18, 42, 29, 489, DateTimeKind.Local).AddTicks(1500), 1104 },
                    { 1040, "659 Carmen st, Brgy. Vasra , Quezon City", "Quezon City", 965544345, "Philippines", "Emmansuelto@gmail.com", "Emman", (short)1, "Suelto", "pogipogiko", "Metro Manila", "CUSTOMER", new DateTime(2021, 7, 8, 18, 42, 29, 489, DateTimeKind.Local).AddTicks(1503), 1128 },
                    { 1041, "438 Balangkas st, Brgy. Arkong bato , Valenzuela City", "Valenzuela City", 907654445, "Philippines", "jasongallera@gmail.com", "jason", (short)1, "gallera", "bhosmapagmahal02", "Metro Manila", "CUSTOMER", new DateTime(2021, 7, 8, 18, 42, 29, 489, DateTimeKind.Local).AddTicks(1508), 1444 },
                    { 1042, "3 Green Meadows st, Brgy. Lawang Bato , Valenzuela City", "Valenzuela City", 998675452, "Philippines", "venjellouolivare@gmail.com", "Venjellou", (short)1, "Olivare", "memerako", "Metro Manila", "CUSTOMER", new DateTime(2021, 7, 8, 18, 42, 29, 489, DateTimeKind.Local).AddTicks(1510), 1447 },
                    { 1043, "5 Marton rd, Brgy. Canumay East , Valenzuela City", "Valenzuela City", 966656778, "Philippines", "jewellmayrena@gmail.com", "Jewell", (short)1, "Mayrena", "bhozmakulit67", "Metro Manila", "CUSTOMER", new DateTime(2021, 7, 8, 18, 42, 29, 489, DateTimeKind.Local).AddTicks(1513), 1447 },
                    { 1044, "8 Chateu Condiminium, Brgy. Canumay West, Valenzuela City", "Valenzuela City", 968565543, "Philippines", "Richardco@gmail.com", "Richard", (short)1, "Co", "tsingtsong2", "Metro Manila", "CUSTOMER", new DateTime(2021, 7, 8, 18, 42, 29, 489, DateTimeKind.Local).AddTicks(1515), 1443 },
                    { 1045, "De Jesus st, Brgy. Gen. T Deleon , Valenzuela City", "Valenzuela City", 962335435, "Philippines", " Jovilyncamaya@gmail.com", "Jovilyn", (short)1, "Camaya", "jubilen23", "Metro Manila", "CUSTOMER", new DateTime(2021, 7, 8, 18, 42, 29, 489, DateTimeKind.Local).AddTicks(1518), 1442 },
                    { 1046, "Langit Rd, Brgy. Bagong Silang , Caloocan City", "Caloocan City", 954434255, "Philippines", "Jemammen@gmail.com", "Jem", (short)1, "Ammen", "onlineselleraq", "Metro Manila", "CUSTOMER", new DateTime(2021, 7, 8, 18, 42, 29, 489, DateTimeKind.Local).AddTicks(1521), 1428 },
                    { 1047, "12 Unang Sigaw, Brgy. Bagong Barrio West , Caloocan City", "Caloocan City", 934343224, "Philippines", "Adraindelrosario@gmail.com", "Adrian", (short)1, "Del Rosario", "bhosztangkad", "Metro Manila", "CUSTOMER", new DateTime(2021, 7, 8, 18, 42, 29, 489, DateTimeKind.Local).AddTicks(1525), 1421 },
                    { 1026, "West Avenue st, Brgy. Phil-Am , Quezon City", "Quezon City", 987654322, "Philippines", "jamtejada@gmail.com", "Jam", (short)1, "Tejada", "aylabredhorse", "Metro Manila", "CUSTOMER", new DateTime(2021, 7, 8, 18, 42, 29, 489, DateTimeKind.Local).AddTicks(1438), 1104 },
                    { 1037, "753 Romblon st, Brgy. Sto. Cristo , Quezon City", "Quezon City", 965430988, "Philippines", "GlenBitiis@gmail.com", "Glen", (short)1, "Bitiis", "bitiisbiot", "Metro Manila", "CUSTOMER", new DateTime(2021, 7, 8, 18, 42, 29, 489, DateTimeKind.Local).AddTicks(1494), 1105 },
                    { 1025, "Lot 3232, Brgy. Paraiso , Quezon City", "Quezon City", 912332122, "Philippines", "markjorieto@gmail.com", "Mark Jorie", (short)1, "Isedera", "baliwako22", "Metro Manila", "CUSTOMER", new DateTime(2021, 7, 8, 18, 42, 29, 489, DateTimeKind.Local).AddTicks(1435), 1104 },
                    { 1012, "22 San Vicente st, Brgy. Damayan, Quezon City", "Quezon City", 923423123, "Philippines", "rendelmarcos@gmail.com", "rendel", (short)1, "marcos", "renedel12", "Metro Manila", "CUSTOMER", new DateTime(2021, 7, 8, 18, 42, 29, 489, DateTimeKind.Local).AddTicks(1400), 1104 },
                    { 1023, "23 Pineda st, Brgy. Pag ibig sa Nayon , Quezon City", "Quezon City", 923322323, "Philippines", "markbarte@gmail.com", "Mark", (short)1, "Barte", "angprobinsyano45", "Metro Manila", "CUSTOMER", new DateTime(2021, 7, 8, 18, 42, 29, 489, DateTimeKind.Local).AddTicks(1430), 1115 },
                    { 1049, "7b Belfast st, Brgy. Kaybiga , Caloocan City", "Caloocan City", 954542442, "Philippines", "Allenmabaling@gmail.com", "Allen", (short)1, "Mabaling", "allentangkad67", "Metro Manila", "CUSTOMER", new DateTime(2021, 7, 8, 18, 42, 29, 489, DateTimeKind.Local).AddTicks(1530), 1420 },
                    { 1000, "10 J.P Rizal st., Brgy. Sta.lucia Novaliches", "Quezon City", 98609346, "Philippines", "liramarie@hotmail.com", "Lira", (short)1, "Marie", "tarantado", "Metro Manila", "CUSTOMER", new DateTime(2021, 7, 8, 18, 42, 29, 489, DateTimeKind.Local).AddTicks(773), 1117 },
                    { 1001, "25 R. Humabon st., Brgy. Sta.lucia Novaliches", "Quezon City", 98609346, "Philippines", "Michaelangelo1@gmail.com", "Michael", (short)1, "Angelo", "09292948752", "Metro Manila", "CUSTOMER", new DateTime(2021, 7, 8, 18, 42, 29, 489, DateTimeKind.Local).AddTicks(1321), 1117 },
                    { 1002, "25 G. Lopez Jaena st., Brgy. Sta.lucia Novaliches", "Quezon City", 98609346, "Philippines", "eot@gmail.com", "Jose Mari", (short)1, "Chan", "bermonths123", "Metro Manila", "CUSTOMER", new DateTime(2021, 7, 8, 18, 42, 29, 489, DateTimeKind.Local).AddTicks(1336), 1117 },
                    { 1003, "506 J.P Rizal st., Brgy. Sta.lucia Novaliches, Quezon City", "Quezon City", 98609346, "Philippines", "santosnisarah20@gmail.com", "Sarah", (short)1, "Santos", "nabatisarah1", "Metro Manila", "CUSTOMER", new DateTime(2021, 7, 8, 18, 42, 29, 489, DateTimeKind.Local).AddTicks(1374), 1117 },
                    { 1004, "11 J. Abad Santos st., Brgy. Sta.lucia Novaliches,Quezon City", "Quezon City", 98609346, "Philippines", "leasalungga22o@gmail.com", "Lea", (short)1, "Salungga", "22osalungga", "Metro Manila", "CUSTOMER", new DateTime(2021, 7, 8, 18, 42, 29, 489, DateTimeKind.Local).AddTicks(1377), 1117 }
                });

            migrationBuilder.InsertData(
                table: "users",
                columns: new[] { "ID", "ADDRESS", "CITY", "CONTACTNO", "COUNTRY", "EMAIL", "FIRSTNAME", "ISACTIVE", "LASTNAME", "PASSWORD", "PROVINCE", "ROLE", "UPDATED_AT", "ZIPCODE" },
                values: new object[,]
                {
                    { 1005, "16 E. Aguinaldo st., Brgy. Sta.lucia Novaliches, Quezon City", "Quezon City", 98609346, "Philippines", "amitafvargas123@gmail.com", "Fatima", (short)1, "Vargas", "memyselfandi", "Metro Manila", "CUSTOMER", new DateTime(2021, 7, 8, 18, 42, 29, 489, DateTimeKind.Local).AddTicks(1380), 1117 },
                    { 1006, "4 Bago Bantay st., Brgy. Alicia, Quezon City", "Quezon City", 932230422, "Philippines", "kengomez5@gmail.com", "Ken", (short)1, "Gomez", "akopogiken", "Metro Manila", "CUSTOMER", new DateTime(2021, 7, 8, 18, 42, 29, 489, DateTimeKind.Local).AddTicks(1382), 1105 },
                    { 1007, "Road 1, Brgy. Bagong Pag Asa, Quezon City", "Quezon City", 965332343, "Philippines", "Jonaldlerum@gmail.com", "Jonald", (short)1, "Lerum", "alabyu2", "Metro Manila", "CUSTOMER", new DateTime(2021, 7, 8, 18, 42, 29, 489, DateTimeKind.Local).AddTicks(1385), 1105 },
                    { 1008, "27 Pluto st, Brgy. Bahay Toro, Project 8, Quezon City", "Quezon City", 943454366, "Philippines", "dickermoran@gmail.com", "Mark Dicker", (short)1, "Moran", "matampuhinaq220", "Metro Manila", "CUSTOMER", new DateTime(2021, 7, 8, 18, 42, 29, 489, DateTimeKind.Local).AddTicks(1389), 1106 },
                    { 1009, "Biak na bato st, Brgy. Balingasa, Quezon City", "Quezon City", 954345224, "Philippines", "rockygolo@gmail.com", "Rocky", (short)1, "Golo", "pogitalagaako", "Metro Manila", "CUSTOMER", new DateTime(2021, 7, 8, 18, 42, 29, 489, DateTimeKind.Local).AddTicks(1392), 1115 },
                    { 1010, "Ana Maria st, Brgy. Bungad, Quezon City", "Quezon City", 923243432, "Philippines", "johnrhodlumabi@gmail.com", "John Rhod", (short)1, "Lumabi", "baliwako12", "Metro Manila", "CUSTOMER", new DateTime(2021, 7, 8, 18, 42, 29, 489, DateTimeKind.Local).AddTicks(1395), 1105 },
                    { 1011, "132 Sgt.Rivera st, Brgy. Damar, Quezon City", "Quezon City", 954234344, "Philippines", "adylegaspi@gmail.com", "Ady", (short)1, "Legaspi", "legaspi123", "Metro Manila", "CUSTOMER", new DateTime(2021, 7, 8, 18, 42, 29, 489, DateTimeKind.Local).AddTicks(1397), 1115 },
                    { 1013, "4 Manalo st, Brgy. San Francisco Del Monte, Quezon City", "Quezon City", 932343321, "Philippines", "rijdandayanan@gmail.com", "Rijelyn", (short)1, "Dandayanan", "dandayananrij45", "Metro Manila", "CUSTOMER", new DateTime(2021, 7, 8, 18, 42, 29, 489, DateTimeKind.Local).AddTicks(1403), 1105 },
                    { 1014, "A. Lopez st, Brgy. Katipunan, Quezon City", "Quezon City", 986776434, "Philippines", "stefmonel@gmail.com", "Steffeny", (short)1, "Monel", "Ilabady123", "Metro Manila", "CUSTOMER", new DateTime(2021, 7, 8, 18, 42, 29, 489, DateTimeKind.Local).AddTicks(1405), 1105 },
                    { 1015, "Calamba st, Brgy. Lourdes, Quezon City", "Quezon City", 943423232, "Philippines", "danamariel@gmail.com", "Dana", (short)1, "Navararro", "danamarie123", "Metro Manila", "CUSTOMER", new DateTime(2021, 7, 8, 18, 42, 29, 489, DateTimeKind.Local).AddTicks(1408), 1114 },
                    { 1016, "33 Kanlaon st, Brgy. Maharlika, Quezon City", "Quezon City", 954233422, "Philippines", "allenmillabas@gmail.com", "Allen", (short)1, "Millabas", "matangkadako12", "Metro Manila", "CUSTOMER", new DateTime(2021, 7, 8, 18, 42, 29, 489, DateTimeKind.Local).AddTicks(1411), 1114 },
                    { 1017, "109 Banawe st, Brgy. Manresa, Quezon City", "Quezon City", 954339593, "Philippines", "jaspergerero@gmail.com", "Jasper", (short)1, "Gerero", "akolange2", "Metro Manila", "CUSTOMER", new DateTime(2021, 7, 8, 18, 42, 29, 489, DateTimeKind.Local).AddTicks(1413), 1115 },
                    { 1018, "15 ignacios st, Brgy. Mariblo, Quezon City", "Quezon City", 955444323, "Philippines", "joshuacelis@gmail.com", "Joshua", (short)1, "Celis", "juswa12345", "Metro Manila", "CUSTOMER", new DateTime(2021, 7, 8, 18, 42, 29, 489, DateTimeKind.Local).AddTicks(1416), 1104 },
                    { 1019, "72 Bahawan st, Brgy. Masambong, Quezon City", "Quezon City", 943255499, "Philippines", "carlchua@gmail.com", "Carl Vincent", (short)1, "Chua", "chuwabels", "Metro Manila", "CUSTOMER", new DateTime(2021, 7, 8, 18, 42, 29, 489, DateTimeKind.Local).AddTicks(1420), 1105 },
                    { 1020, "45 Angelo st, Brgy. NS Amoranto , Quezon City", "Quezon City", 912332122, "Philippines", "cathymaninang@gmail.com", "Cathy", (short)1, "Maninang", "Exkosihoganpogi", "Metro Manila", "CUSTOMER", new DateTime(2021, 7, 8, 18, 42, 29, 489, DateTimeKind.Local).AddTicks(1422), 1114 },
                    { 1021, "37 Zamboanga st, Brgy. Nayong Kanluran , Quezon City", "Quezon City", 966776809, "Philippines", "clarrisevasquez@gmail.com", "Clarisse", (short)1, "Vasquez", "aylabregis24", "Metro Manila", "CUSTOMER", new DateTime(2021, 7, 8, 18, 42, 29, 489, DateTimeKind.Local).AddTicks(1425), 1104 },
                    { 1022, "35 Iriga st, Brgy. Paang Bundok , Quezon City", "Quezon City", 953320324, "Philippines", "krenzcamacho@gmail.com", "Krenz", (short)1, "Camacho", "Mahalkosikuyapat", "Metro Manila", "CUSTOMER", new DateTime(2021, 7, 8, 18, 42, 29, 489, DateTimeKind.Local).AddTicks(1427), 1114 },
                    { 1024, "Basa st, Brgy. Paltok , Quezon City", "Quezon City", 915643534, "Philippines", "jonnasalo@gmail.com", "Jonna mae", (short)1, "Salo", "bebekosihogan66", "Metro Manila", "CUSTOMER", new DateTime(2021, 7, 8, 18, 42, 29, 489, DateTimeKind.Local).AddTicks(1433), 1105 },
                    { 1050, "89 Jade st, Brgy. Deparo , Caloocan City", "Caloocan City", 954543450, "Philippines", "Carlvincenttabin@gmail.com", "Carl Vincent", (short)1, "Tabin", "boijollibee", "Metro Manila", "CUSTOMER", new DateTime(2021, 7, 8, 18, 42, 29, 489, DateTimeKind.Local).AddTicks(1533), 1420 }
                });

            migrationBuilder.InsertData(
                table: "product_details",
                columns: new[] { "ID", "ADDITIONAL_INFO", "IMAGE1", "IMAGE2", "IMAGE3", "IMAGE4", "IMAGE5", "IS_ACTIVE", "PRODUCT_ID", "PRODUCT_TYPE_ID", "UPDATED_AT" },
                values: new object[,]
                {
                    { 1, "Define your Size, Color or Anything that occupied space and has mass.", "/images/Oppo/prod-1.png", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", (short)1, 1, 1, new DateTime(2021, 7, 8, 18, 42, 29, 487, DateTimeKind.Local).AddTicks(2117) },
                    { 28, "Define your Size, Color or Anything that occupied space and has mass.", "/images/Apple/prod-28.png", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", (short)1, 28, 1, new DateTime(2021, 7, 8, 18, 42, 29, 487, DateTimeKind.Local).AddTicks(9718) },
                    { 29, "Define your Size, Color or Anything that occupied space and has mass.", "/images/Apple/prod-29.png", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", (short)1, 29, 1, new DateTime(2021, 7, 8, 18, 42, 29, 487, DateTimeKind.Local).AddTicks(9721) },
                    { 30, "Define your Size, Color or Anything that occupied space and has mass.", "/images/Apple/prod-30.png", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", (short)1, 30, 1, new DateTime(2021, 7, 8, 18, 42, 29, 487, DateTimeKind.Local).AddTicks(9724) },
                    { 31, "Define your Size, Color or Anything that occupied space and has mass.", "/images/Realme/prod-31.png", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", (short)1, 31, 1, new DateTime(2021, 7, 8, 18, 42, 29, 487, DateTimeKind.Local).AddTicks(9726) },
                    { 32, "Define your Size, Color or Anything that occupied space and has mass.", "/images/Realme/prod-32.png", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", (short)1, 32, 1, new DateTime(2021, 7, 8, 18, 42, 29, 487, DateTimeKind.Local).AddTicks(9788) },
                    { 33, "Define your Size, Color or Anything that occupied space and has mass.", "/images/Realme/prod-33.png", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", (short)1, 33, 1, new DateTime(2021, 7, 8, 18, 42, 29, 487, DateTimeKind.Local).AddTicks(9793) },
                    { 34, "Define your Size, Color or Anything that occupied space and has mass.", "/images/Realme/prod-34.png", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", (short)1, 34, 1, new DateTime(2021, 7, 8, 18, 42, 29, 487, DateTimeKind.Local).AddTicks(9795) },
                    { 35, "Define your Size, Color or Anything that occupied space and has mass.", "/images/Realme/prod-35.png", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", (short)1, 35, 1, new DateTime(2021, 7, 8, 18, 42, 29, 487, DateTimeKind.Local).AddTicks(9798) },
                    { 36, "Define your Size, Color or Anything that occupied space and has mass.", "/images/Realme/prod-36.png", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", (short)1, 36, 1, new DateTime(2021, 7, 8, 18, 42, 29, 487, DateTimeKind.Local).AddTicks(9800) },
                    { 37, "Define your Size, Color or Anything that occupied space and has mass.", "/images/Realme/prod-37.png", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", (short)1, 37, 1, new DateTime(2021, 7, 8, 18, 42, 29, 487, DateTimeKind.Local).AddTicks(9803) },
                    { 38, "Define your Size, Color or Anything that occupied space and has mass.", "/images/Realme/prod-38.png", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", (short)1, 38, 1, new DateTime(2021, 7, 8, 18, 42, 29, 487, DateTimeKind.Local).AddTicks(9805) },
                    { 39, "Define your Size, Color or Anything that occupied space and has mass.", "/images/Realme/prod-39.png", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", (short)1, 39, 1, new DateTime(2021, 7, 8, 18, 42, 29, 487, DateTimeKind.Local).AddTicks(9807) },
                    { 40, "Define your Size, Color or Anything that occupied space and has mass.", "/images/Realme/prod-40.png", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", (short)1, 40, 1, new DateTime(2021, 7, 8, 18, 42, 29, 487, DateTimeKind.Local).AddTicks(9810) },
                    { 41, "Define your Size, Color or Anything that occupied space and has mass.", "/images/Huawei/prod-41.png", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", (short)1, 41, 1, new DateTime(2021, 7, 8, 18, 42, 29, 487, DateTimeKind.Local).AddTicks(9813) },
                    { 42, "Define your Size, Color or Anything that occupied space and has mass.", "/images/Huawei/prod-42.png", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", (short)1, 42, 1, new DateTime(2021, 7, 8, 18, 42, 29, 487, DateTimeKind.Local).AddTicks(9816) },
                    { 43, "Define your Size, Color or Anything that occupied space and has mass.", "/images/Huawei/prod-43.png", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", (short)1, 43, 1, new DateTime(2021, 7, 8, 18, 42, 29, 487, DateTimeKind.Local).AddTicks(9818) },
                    { 44, "Define your Size, Color or Anything that occupied space and has mass.", "/images/Huawei/prod-44.png", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", (short)1, 44, 1, new DateTime(2021, 7, 8, 18, 42, 29, 487, DateTimeKind.Local).AddTicks(9821) },
                    { 45, "Define your Size, Color or Anything that occupied space and has mass.", "/images/Huawei/prod-45.png", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", (short)1, 45, 1, new DateTime(2021, 7, 8, 18, 42, 29, 487, DateTimeKind.Local).AddTicks(9823) },
                    { 46, "Define your Size, Color or Anything that occupied space and has mass.", "/images/Huawei/prod-46.png", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", (short)1, 46, 1, new DateTime(2021, 7, 8, 18, 42, 29, 487, DateTimeKind.Local).AddTicks(9825) },
                    { 47, "Define your Size, Color or Anything that occupied space and has mass.", "/images/Huawei/prod-47.png", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", (short)1, 47, 1, new DateTime(2021, 7, 8, 18, 42, 29, 487, DateTimeKind.Local).AddTicks(9828) },
                    { 48, "Define your Size, Color or Anything that occupied space and has mass.", "/images/Huawei/prod-48.png", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", (short)1, 48, 1, new DateTime(2021, 7, 8, 18, 42, 29, 487, DateTimeKind.Local).AddTicks(9830) },
                    { 27, "Define your Size, Color or Anything that occupied space and has mass.", "/images/Apple/prod-27.png", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", (short)1, 27, 1, new DateTime(2021, 7, 8, 18, 42, 29, 487, DateTimeKind.Local).AddTicks(9716) },
                    { 26, "Define your Size, Color or Anything that occupied space and has mass.", "/images/Apple/prod-26.png", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", (short)1, 26, 1, new DateTime(2021, 7, 8, 18, 42, 29, 487, DateTimeKind.Local).AddTicks(9714) },
                    { 25, "Define your Size, Color or Anything that occupied space and has mass.", "/images/Apple/prod-25.png", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", (short)1, 25, 1, new DateTime(2021, 7, 8, 18, 42, 29, 487, DateTimeKind.Local).AddTicks(9711) },
                    { 24, "Define your Size, Color or Anything that occupied space and has mass.", "/images/Apple/prod-24.png", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", (short)1, 24, 1, new DateTime(2021, 7, 8, 18, 42, 29, 487, DateTimeKind.Local).AddTicks(9709) },
                    { 2, "Define your Size, Color or Anything that occupied space and has mass.", "/images/Oppo/prod-2.png", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", (short)1, 2, 1, new DateTime(2021, 7, 8, 18, 42, 29, 487, DateTimeKind.Local).AddTicks(9625) },
                    { 3, "Define your Size, Color or Anything that occupied space and has mass.", "/images/Oppo/prod-3.png", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", (short)1, 3, 1, new DateTime(2021, 7, 8, 18, 42, 29, 487, DateTimeKind.Local).AddTicks(9654) },
                    { 4, "Define your Size, Color or Anything that occupied space and has mass.", "/images/Oppo/prod-4.png", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", (short)1, 4, 1, new DateTime(2021, 7, 8, 18, 42, 29, 487, DateTimeKind.Local).AddTicks(9658) },
                    { 5, "Define your Size, Color or Anything that occupied space and has mass.", "/images/Oppo/prod-5.png", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", (short)1, 5, 1, new DateTime(2021, 7, 8, 18, 42, 29, 487, DateTimeKind.Local).AddTicks(9661) },
                    { 6, "Define your Size, Color or Anything that occupied space and has mass.", "/images/Oppo/prod-6.png", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", (short)1, 6, 1, new DateTime(2021, 7, 8, 18, 42, 29, 487, DateTimeKind.Local).AddTicks(9664) },
                    { 7, "Define your Size, Color or Anything that occupied space and has mass.", "/images/Oppo/prod-7.png", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", (short)1, 7, 1, new DateTime(2021, 7, 8, 18, 42, 29, 487, DateTimeKind.Local).AddTicks(9666) },
                    { 8, "Define your Size, Color or Anything that occupied space and has mass.", "/images/Oppo/prod-8.png", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", (short)1, 8, 1, new DateTime(2021, 7, 8, 18, 42, 29, 487, DateTimeKind.Local).AddTicks(9668) },
                    { 9, "Define your Size, Color or Anything that occupied space and has mass.", "/images/Oppo/prod-9.png", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", (short)1, 9, 1, new DateTime(2021, 7, 8, 18, 42, 29, 487, DateTimeKind.Local).AddTicks(9671) },
                    { 10, "Define your Size, Color or Anything that occupied space and has mass.", "/images/Oppo/prod-10.png", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", (short)1, 10, 1, new DateTime(2021, 7, 8, 18, 42, 29, 487, DateTimeKind.Local).AddTicks(9673) },
                    { 11, "Define your Size, Color or Anything that occupied space and has mass.", "/images/Samsung/prod-11.png", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", (short)1, 11, 1, new DateTime(2021, 7, 8, 18, 42, 29, 487, DateTimeKind.Local).AddTicks(9676) },
                    { 49, "Define your Size, Color or Anything that occupied space and has mass.", "/images/Huawei/prod-49.png", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", (short)1, 49, 1, new DateTime(2021, 7, 8, 18, 42, 29, 487, DateTimeKind.Local).AddTicks(9833) },
                    { 12, "Define your Size, Color or Anything that occupied space and has mass.", "/images/Samsung/prod-12.png", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", (short)1, 12, 1, new DateTime(2021, 7, 8, 18, 42, 29, 487, DateTimeKind.Local).AddTicks(9678) },
                    { 14, "Define your Size, Color or Anything that occupied space and has mass.", "/images/Samsung/prod-14.png", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", (short)1, 14, 1, new DateTime(2021, 7, 8, 18, 42, 29, 487, DateTimeKind.Local).AddTicks(9683) },
                    { 15, "Define your Size, Color or Anything that occupied space and has mass.", "/images/Samsung/prod-15.png", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", (short)1, 15, 1, new DateTime(2021, 7, 8, 18, 42, 29, 487, DateTimeKind.Local).AddTicks(9686) },
                    { 16, "Define your Size, Color or Anything that occupied space and has mass.", "/images/Samsung/prod-16.png", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", (short)1, 16, 1, new DateTime(2021, 7, 8, 18, 42, 29, 487, DateTimeKind.Local).AddTicks(9688) },
                    { 17, "Define your Size, Color or Anything that occupied space and has mass.", "/images/Samsung/prod-17.png", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", (short)1, 17, 1, new DateTime(2021, 7, 8, 18, 42, 29, 487, DateTimeKind.Local).AddTicks(9691) }
                });

            migrationBuilder.InsertData(
                table: "product_details",
                columns: new[] { "ID", "ADDITIONAL_INFO", "IMAGE1", "IMAGE2", "IMAGE3", "IMAGE4", "IMAGE5", "IS_ACTIVE", "PRODUCT_ID", "PRODUCT_TYPE_ID", "UPDATED_AT" },
                values: new object[,]
                {
                    { 18, "Define your Size, Color or Anything that occupied space and has mass.", "/images/Samsung/prod-18.png", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", (short)1, 18, 1, new DateTime(2021, 7, 8, 18, 42, 29, 487, DateTimeKind.Local).AddTicks(9694) },
                    { 19, "Define your Size, Color or Anything that occupied space and has mass.", "/images/Samsung/prod-19.png", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", (short)1, 19, 1, new DateTime(2021, 7, 8, 18, 42, 29, 487, DateTimeKind.Local).AddTicks(9696) },
                    { 20, "Define your Size, Color or Anything that occupied space and has mass.", "/images/Samsung/prod-20.png", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", (short)1, 20, 1, new DateTime(2021, 7, 8, 18, 42, 29, 487, DateTimeKind.Local).AddTicks(9699) },
                    { 21, "Define your Size, Color or Anything that occupied space and has mass.", "/images/Apple/prod-21.png", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", (short)1, 21, 1, new DateTime(2021, 7, 8, 18, 42, 29, 487, DateTimeKind.Local).AddTicks(9701) },
                    { 22, "Define your Size, Color or Anything that occupied space and has mass.", "/images/Apple/prod-22.png", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", (short)1, 22, 1, new DateTime(2021, 7, 8, 18, 42, 29, 487, DateTimeKind.Local).AddTicks(9704) },
                    { 23, "Define your Size, Color or Anything that occupied space and has mass.", "/images/Apple/prod-23.png", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", (short)1, 23, 1, new DateTime(2021, 7, 8, 18, 42, 29, 487, DateTimeKind.Local).AddTicks(9706) },
                    { 13, "Define your Size, Color or Anything that occupied space and has mass.", "/images/Samsung/prod-13.png", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", (short)1, 13, 1, new DateTime(2021, 7, 8, 18, 42, 29, 487, DateTimeKind.Local).AddTicks(9681) },
                    { 50, "Define your Size, Color or Anything that occupied space and has mass.", "/images/Huawei/prod-50.png", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", "/images/DEFAULT_IMAGE.jpg", (short)1, 50, 1, new DateTime(2021, 7, 8, 18, 42, 29, 487, DateTimeKind.Local).AddTicks(9835) }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "product_details",
                keyColumn: "ID",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "product_details",
                keyColumn: "ID",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "product_details",
                keyColumn: "ID",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "product_details",
                keyColumn: "ID",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "product_details",
                keyColumn: "ID",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "product_details",
                keyColumn: "ID",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "product_details",
                keyColumn: "ID",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "product_details",
                keyColumn: "ID",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "product_details",
                keyColumn: "ID",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "product_details",
                keyColumn: "ID",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "product_details",
                keyColumn: "ID",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "product_details",
                keyColumn: "ID",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "product_details",
                keyColumn: "ID",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "product_details",
                keyColumn: "ID",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "product_details",
                keyColumn: "ID",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "product_details",
                keyColumn: "ID",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "product_details",
                keyColumn: "ID",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "product_details",
                keyColumn: "ID",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "product_details",
                keyColumn: "ID",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "product_details",
                keyColumn: "ID",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "product_details",
                keyColumn: "ID",
                keyValue: 21);

            migrationBuilder.DeleteData(
                table: "product_details",
                keyColumn: "ID",
                keyValue: 22);

            migrationBuilder.DeleteData(
                table: "product_details",
                keyColumn: "ID",
                keyValue: 23);

            migrationBuilder.DeleteData(
                table: "product_details",
                keyColumn: "ID",
                keyValue: 24);

            migrationBuilder.DeleteData(
                table: "product_details",
                keyColumn: "ID",
                keyValue: 25);

            migrationBuilder.DeleteData(
                table: "product_details",
                keyColumn: "ID",
                keyValue: 26);

            migrationBuilder.DeleteData(
                table: "product_details",
                keyColumn: "ID",
                keyValue: 27);

            migrationBuilder.DeleteData(
                table: "product_details",
                keyColumn: "ID",
                keyValue: 28);

            migrationBuilder.DeleteData(
                table: "product_details",
                keyColumn: "ID",
                keyValue: 29);

            migrationBuilder.DeleteData(
                table: "product_details",
                keyColumn: "ID",
                keyValue: 30);

            migrationBuilder.DeleteData(
                table: "product_details",
                keyColumn: "ID",
                keyValue: 31);

            migrationBuilder.DeleteData(
                table: "product_details",
                keyColumn: "ID",
                keyValue: 32);

            migrationBuilder.DeleteData(
                table: "product_details",
                keyColumn: "ID",
                keyValue: 33);

            migrationBuilder.DeleteData(
                table: "product_details",
                keyColumn: "ID",
                keyValue: 34);

            migrationBuilder.DeleteData(
                table: "product_details",
                keyColumn: "ID",
                keyValue: 35);

            migrationBuilder.DeleteData(
                table: "product_details",
                keyColumn: "ID",
                keyValue: 36);

            migrationBuilder.DeleteData(
                table: "product_details",
                keyColumn: "ID",
                keyValue: 37);

            migrationBuilder.DeleteData(
                table: "product_details",
                keyColumn: "ID",
                keyValue: 38);

            migrationBuilder.DeleteData(
                table: "product_details",
                keyColumn: "ID",
                keyValue: 39);

            migrationBuilder.DeleteData(
                table: "product_details",
                keyColumn: "ID",
                keyValue: 40);

            migrationBuilder.DeleteData(
                table: "product_details",
                keyColumn: "ID",
                keyValue: 41);

            migrationBuilder.DeleteData(
                table: "product_details",
                keyColumn: "ID",
                keyValue: 42);

            migrationBuilder.DeleteData(
                table: "product_details",
                keyColumn: "ID",
                keyValue: 43);

            migrationBuilder.DeleteData(
                table: "product_details",
                keyColumn: "ID",
                keyValue: 44);

            migrationBuilder.DeleteData(
                table: "product_details",
                keyColumn: "ID",
                keyValue: 45);

            migrationBuilder.DeleteData(
                table: "product_details",
                keyColumn: "ID",
                keyValue: 46);

            migrationBuilder.DeleteData(
                table: "product_details",
                keyColumn: "ID",
                keyValue: 47);

            migrationBuilder.DeleteData(
                table: "product_details",
                keyColumn: "ID",
                keyValue: 48);

            migrationBuilder.DeleteData(
                table: "product_details",
                keyColumn: "ID",
                keyValue: 49);

            migrationBuilder.DeleteData(
                table: "product_details",
                keyColumn: "ID",
                keyValue: 50);

            migrationBuilder.DeleteData(
                table: "product_type",
                keyColumn: "ID",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "users",
                keyColumn: "ID",
                keyValue: 1000);

            migrationBuilder.DeleteData(
                table: "users",
                keyColumn: "ID",
                keyValue: 1001);

            migrationBuilder.DeleteData(
                table: "users",
                keyColumn: "ID",
                keyValue: 1002);

            migrationBuilder.DeleteData(
                table: "users",
                keyColumn: "ID",
                keyValue: 1003);

            migrationBuilder.DeleteData(
                table: "users",
                keyColumn: "ID",
                keyValue: 1004);

            migrationBuilder.DeleteData(
                table: "users",
                keyColumn: "ID",
                keyValue: 1005);

            migrationBuilder.DeleteData(
                table: "users",
                keyColumn: "ID",
                keyValue: 1006);

            migrationBuilder.DeleteData(
                table: "users",
                keyColumn: "ID",
                keyValue: 1007);

            migrationBuilder.DeleteData(
                table: "users",
                keyColumn: "ID",
                keyValue: 1008);

            migrationBuilder.DeleteData(
                table: "users",
                keyColumn: "ID",
                keyValue: 1009);

            migrationBuilder.DeleteData(
                table: "users",
                keyColumn: "ID",
                keyValue: 1010);

            migrationBuilder.DeleteData(
                table: "users",
                keyColumn: "ID",
                keyValue: 1011);

            migrationBuilder.DeleteData(
                table: "users",
                keyColumn: "ID",
                keyValue: 1012);

            migrationBuilder.DeleteData(
                table: "users",
                keyColumn: "ID",
                keyValue: 1013);

            migrationBuilder.DeleteData(
                table: "users",
                keyColumn: "ID",
                keyValue: 1014);

            migrationBuilder.DeleteData(
                table: "users",
                keyColumn: "ID",
                keyValue: 1015);

            migrationBuilder.DeleteData(
                table: "users",
                keyColumn: "ID",
                keyValue: 1016);

            migrationBuilder.DeleteData(
                table: "users",
                keyColumn: "ID",
                keyValue: 1017);

            migrationBuilder.DeleteData(
                table: "users",
                keyColumn: "ID",
                keyValue: 1018);

            migrationBuilder.DeleteData(
                table: "users",
                keyColumn: "ID",
                keyValue: 1019);

            migrationBuilder.DeleteData(
                table: "users",
                keyColumn: "ID",
                keyValue: 1020);

            migrationBuilder.DeleteData(
                table: "users",
                keyColumn: "ID",
                keyValue: 1021);

            migrationBuilder.DeleteData(
                table: "users",
                keyColumn: "ID",
                keyValue: 1022);

            migrationBuilder.DeleteData(
                table: "users",
                keyColumn: "ID",
                keyValue: 1023);

            migrationBuilder.DeleteData(
                table: "users",
                keyColumn: "ID",
                keyValue: 1024);

            migrationBuilder.DeleteData(
                table: "users",
                keyColumn: "ID",
                keyValue: 1025);

            migrationBuilder.DeleteData(
                table: "users",
                keyColumn: "ID",
                keyValue: 1026);

            migrationBuilder.DeleteData(
                table: "users",
                keyColumn: "ID",
                keyValue: 1027);

            migrationBuilder.DeleteData(
                table: "users",
                keyColumn: "ID",
                keyValue: 1028);

            migrationBuilder.DeleteData(
                table: "users",
                keyColumn: "ID",
                keyValue: 1029);

            migrationBuilder.DeleteData(
                table: "users",
                keyColumn: "ID",
                keyValue: 1030);

            migrationBuilder.DeleteData(
                table: "users",
                keyColumn: "ID",
                keyValue: 1031);

            migrationBuilder.DeleteData(
                table: "users",
                keyColumn: "ID",
                keyValue: 1032);

            migrationBuilder.DeleteData(
                table: "users",
                keyColumn: "ID",
                keyValue: 1033);

            migrationBuilder.DeleteData(
                table: "users",
                keyColumn: "ID",
                keyValue: 1034);

            migrationBuilder.DeleteData(
                table: "users",
                keyColumn: "ID",
                keyValue: 1035);

            migrationBuilder.DeleteData(
                table: "users",
                keyColumn: "ID",
                keyValue: 1036);

            migrationBuilder.DeleteData(
                table: "users",
                keyColumn: "ID",
                keyValue: 1037);

            migrationBuilder.DeleteData(
                table: "users",
                keyColumn: "ID",
                keyValue: 1038);

            migrationBuilder.DeleteData(
                table: "users",
                keyColumn: "ID",
                keyValue: 1039);

            migrationBuilder.DeleteData(
                table: "users",
                keyColumn: "ID",
                keyValue: 1040);

            migrationBuilder.DeleteData(
                table: "users",
                keyColumn: "ID",
                keyValue: 1041);

            migrationBuilder.DeleteData(
                table: "users",
                keyColumn: "ID",
                keyValue: 1042);

            migrationBuilder.DeleteData(
                table: "users",
                keyColumn: "ID",
                keyValue: 1043);

            migrationBuilder.DeleteData(
                table: "users",
                keyColumn: "ID",
                keyValue: 1044);

            migrationBuilder.DeleteData(
                table: "users",
                keyColumn: "ID",
                keyValue: 1045);

            migrationBuilder.DeleteData(
                table: "users",
                keyColumn: "ID",
                keyValue: 1046);

            migrationBuilder.DeleteData(
                table: "users",
                keyColumn: "ID",
                keyValue: 1047);

            migrationBuilder.DeleteData(
                table: "users",
                keyColumn: "ID",
                keyValue: 1048);

            migrationBuilder.DeleteData(
                table: "users",
                keyColumn: "ID",
                keyValue: 1049);

            migrationBuilder.DeleteData(
                table: "users",
                keyColumn: "ID",
                keyValue: 1050);

            migrationBuilder.DeleteData(
                table: "product_type",
                keyColumn: "ID",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "products",
                keyColumn: "ID",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "products",
                keyColumn: "ID",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "products",
                keyColumn: "ID",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "products",
                keyColumn: "ID",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "products",
                keyColumn: "ID",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "products",
                keyColumn: "ID",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "products",
                keyColumn: "ID",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "products",
                keyColumn: "ID",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "products",
                keyColumn: "ID",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "products",
                keyColumn: "ID",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "products",
                keyColumn: "ID",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "products",
                keyColumn: "ID",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "products",
                keyColumn: "ID",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "products",
                keyColumn: "ID",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "products",
                keyColumn: "ID",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "products",
                keyColumn: "ID",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "products",
                keyColumn: "ID",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "products",
                keyColumn: "ID",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "products",
                keyColumn: "ID",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "products",
                keyColumn: "ID",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "products",
                keyColumn: "ID",
                keyValue: 21);

            migrationBuilder.DeleteData(
                table: "products",
                keyColumn: "ID",
                keyValue: 22);

            migrationBuilder.DeleteData(
                table: "products",
                keyColumn: "ID",
                keyValue: 23);

            migrationBuilder.DeleteData(
                table: "products",
                keyColumn: "ID",
                keyValue: 24);

            migrationBuilder.DeleteData(
                table: "products",
                keyColumn: "ID",
                keyValue: 25);

            migrationBuilder.DeleteData(
                table: "products",
                keyColumn: "ID",
                keyValue: 26);

            migrationBuilder.DeleteData(
                table: "products",
                keyColumn: "ID",
                keyValue: 27);

            migrationBuilder.DeleteData(
                table: "products",
                keyColumn: "ID",
                keyValue: 28);

            migrationBuilder.DeleteData(
                table: "products",
                keyColumn: "ID",
                keyValue: 29);

            migrationBuilder.DeleteData(
                table: "products",
                keyColumn: "ID",
                keyValue: 30);

            migrationBuilder.DeleteData(
                table: "products",
                keyColumn: "ID",
                keyValue: 31);

            migrationBuilder.DeleteData(
                table: "products",
                keyColumn: "ID",
                keyValue: 32);

            migrationBuilder.DeleteData(
                table: "products",
                keyColumn: "ID",
                keyValue: 33);

            migrationBuilder.DeleteData(
                table: "products",
                keyColumn: "ID",
                keyValue: 34);

            migrationBuilder.DeleteData(
                table: "products",
                keyColumn: "ID",
                keyValue: 35);

            migrationBuilder.DeleteData(
                table: "products",
                keyColumn: "ID",
                keyValue: 36);

            migrationBuilder.DeleteData(
                table: "products",
                keyColumn: "ID",
                keyValue: 37);

            migrationBuilder.DeleteData(
                table: "products",
                keyColumn: "ID",
                keyValue: 38);

            migrationBuilder.DeleteData(
                table: "products",
                keyColumn: "ID",
                keyValue: 39);

            migrationBuilder.DeleteData(
                table: "products",
                keyColumn: "ID",
                keyValue: 40);

            migrationBuilder.DeleteData(
                table: "products",
                keyColumn: "ID",
                keyValue: 41);

            migrationBuilder.DeleteData(
                table: "products",
                keyColumn: "ID",
                keyValue: 42);

            migrationBuilder.DeleteData(
                table: "products",
                keyColumn: "ID",
                keyValue: 43);

            migrationBuilder.DeleteData(
                table: "products",
                keyColumn: "ID",
                keyValue: 44);

            migrationBuilder.DeleteData(
                table: "products",
                keyColumn: "ID",
                keyValue: 45);

            migrationBuilder.DeleteData(
                table: "products",
                keyColumn: "ID",
                keyValue: 46);

            migrationBuilder.DeleteData(
                table: "products",
                keyColumn: "ID",
                keyValue: 47);

            migrationBuilder.DeleteData(
                table: "products",
                keyColumn: "ID",
                keyValue: 48);

            migrationBuilder.DeleteData(
                table: "products",
                keyColumn: "ID",
                keyValue: 49);

            migrationBuilder.DeleteData(
                table: "products",
                keyColumn: "ID",
                keyValue: 50);
        }
    }
}
